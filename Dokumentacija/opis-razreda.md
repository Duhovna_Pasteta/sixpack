# Razredi

**Neki nazivi atributa i metoda su dugački da se razumije na što se misli. U stvarnoj implementaciji mogu biti skraćeni.**

## Opisi razreda

* Autoservis
  - Atributi: naziv autoservisa, radno vrijeme, adresa, broj telefona, interval zaprimanja vozila, interval preuzimanja vozila, lista slobodnih zamjenskih vozila, lista zauzetih zamjenskih vozila, lista predefiniranih usluga autoservisa, lista slobodnih termina, lista zauzetih termina, baza podataka (_referenca na objekt razreda Baza podataka da može dohvaćati podatke_)
  - Operacija: dohvat općih informacija autoservisa, promjena općih informacija autoservisa
  - Metode:
    - `getNazivAutoservisa()` i ostali getteri
    - `setNazivAutoservisa()` i ostali setteri
    - `dodajSlobodnoZamjenskoVozilo()`
    - `rezervirajSlobodnoZamjenskoVozilo()`
    - `oslobodiZauzetoZamjenskoVozilo()`
    - `dohvatiListuPredefiniranihUslugaAutoservisa()`
    - `dodajPredefiniranuUsluguAutoservisa()`
    - `ukloniPredefiniranuUsluguAutoservisa()`
    - `dohvatiListuSlobodnihTermina()` - Praktički vraća se samo lista vremena u kojima se termin može zakazati (kreirati objekt s tim vremenom)
    - `dohvatiListuZauzetihTermina()` - Vraća listu objekata razreda Termin
    - `rezervirajSlobodniTermin()`
    - `oslobodiZauzetiTermin()`
* Apstraktni korisnik
  - Atributi: ime, prezime, korisničko ime (isto kao i e-mail), lozinka, e-mail, broj telefona, adresa
  - Operacije: prikaz korisničkih informacija (getteri), promjena korisničkih informacija (setteri)
  - Metode:
    - `getImeKorisnika()` i ostali getteri
    - `setImeKorisnika()` i ostali setteri
* Korisnik (Podrazumijeva se registrirani korisnik za kojeg su pohranjeni podaci u bazi podataka)
  - Atributi: lista priduženih termina, lista pridruženih zamjenskih vozila
  - Operacije: dohvati pridružene termine, dohvati pridružena zamjenska vozila
  - Metode:
    - `dohvatListuPridruženihTermina()`
    - `dohvatiListuZamjenskihVozila()`
* Serviser
  - Atributi: lista pridruženih termina servisera
  - Operacije: dohvati pridružene termine servisera
  - Metode:
    - `dohvatiListuPridruženihTermina()`
* Administrator
  - Atributi:
  - Operacije:
* Baza podataka (ovaj će razred uglavnom koristiti razred Autoservis)
  - Atributi: IP-adresa, pristupni podaci (username, password)
  - Operacije: prikaz i promjena IP-adrese, dohvaćanje podataka o cjelokupnom autoservisu (iz baze podataka):
    - Dohvaćanje liste korisnika
    - Dohvaćanje liste servisera
    - Dohvaćanje liste termina
    - Dohvaćanje liste zamjenskih vozila
    - Unos termina, zamjenskih vozila, korisnika itd.
  - Metode:
    - `getIPAddress()` i ostali getteri atributa
    - `setIPAddress()` i ostali setteri atributa
    - `dohvatiListuKorisnika()` i ostale metode sličnog tipa
* Usluga
  - Atributi: kratko ime usluge, opis usluge (dugi tekst)
  - Operacije: prikaz i promjena atributa usluge
  - Metode:
    - `getImeUsluge()` i ostali getteri
    - `setImeUsluge()` i ostali setteri
* Termin
  - Atributi: datum, vrijeme, korisnik, serviser, lista komentara, potvrda zaprimanja vozila
  - Operacije: prikaz i promjena gornjih informacija, dodavanje komentara
  - Metode:
    - `getDatum()` i ostali getteri
    - `setDatum()` i ostali setteri
    - `dodajKomentar()`
    - `generirajPovrduZaprimanjaVozila()` - Generira se potvrda na temelju atributa ovog razreda Termin.
    - `dohvatiPotvrduZaprimanjaVozila()`
* Vozilo
  - Atributi: proizvođač (_ovo je samo da generaliziramo sustav, svi će automobili biti istog proizvođača kod ovog autoservisa_), model, godina proizvodnje, registarska oznaka
  - Operacije: dohvat i promjena ovih gore informacija (getteri i setteri)
  - Metode:
    - `getModelVozila()` i ostali getteri
    - `setModelVozila()` i ostali setteri
* Komentar
  - Atributi: tekst komentara, datum, vrijeme, autor (apstraktni korisnik)
  - Operacije: prikaz i promjena atributa komentara
  - Metode:
    - `getTekstKomentara()` i ostali getteri
    - `setTekstKomentara()` i ostali setteri

## Povezivanje razreda

### Asocijacija

* Korisniku može biti dodijeljeno vozilo (0 ili 1)

### Kompozicija

* Termin se sastoji od korisnika, usluge i servisera
* Komentar se sastoji od apstraktnog korisnika

### Agregacija

* Autoservis sadržava termine, usluge, vozila (0 ili više)
* Serviser sadržava pridružene mu termine (0 ili više)
* Termin sadržava komentare (0 ili više)

### Generalizacija

* Administrator, Korisnik i Serviser naslijeđuju apstraktnu klasu Apstraktni korisnik

### Ovisnost

* Autoservis ovisi o bazi podataka

_Pisano markdownom._
