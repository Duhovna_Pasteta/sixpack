import React, {Component} from 'react';
import Form from '../../Form/Form';
import Button from '../../Button/Button';
import cookie from "react-cookies";
import Container from "../../Container/Container";
import {Link} from "react-router-dom";

class SingleAppointment extends Component{

    state = {
        data: [],
        carIssues: [],
        client: [],
        servicer: [],
        comments: [],
        appointmentDateTime: '',
        additionalUserDescription: '',
        comment: '',
        vehicleStatus: ''
    }

    componentWillMount(){
        if(cookie.load('role') === "ROLE_USER")
            this.props.history.push("/stranica_nije_pronadena");

        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
            }
        };

        fetch(`/api/appointment/${this.props.match.params.id}`, options)
            .then(response => {
                if(response.status === 400)
                    this.props.history.push('/stranica_nije_pronadena');
            })
    }

    componentDidMount()     {
        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
            }
        };

        fetch(`/api/appointment/${this.props.match.params.id}`, options)
            .then(data => data.json())
            .then(json => this.setState ( {carIssues: json.carIssues, client: json.client, comments: json.comments,
                appointmentDateTime: json.appointmentDateTime, servicer: json.repairer, vehicleStatus: json.carStatus,
                additionalUserDescription: json.additionalUserDescription, data:json}));
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    delete = () => {
        const options = {
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
            }
        };

        fetch(`/api/appointment/${this.props.match.params.id}`, options);
        this.props.history.push('/listatermina');
        alert('Uspješno ste obrisali termin');
    }

    render(){
        console.log(this.state);
        return(
            <Container title = 'Termin'>
                <Form onSubmit = {this.onSubmit}>
                    <Form.Row label = 'Vrijeme'>
                        <input type = 'text' disabled value = {this.state.appointmentDateTime}/>
                    </Form.Row>

                    <Form.Row label = 'Status vozila'>
                        <input type = 'text' disabled value = {this.state.vehicleStatus}/>
                    </Form.Row>

                    <Form.Row label = 'Problemi s autom'>
                        {this.state.carIssues.map((issue) => <input type = "text" disabled key = {issue.id} value = {issue.description}/>)}
                    </Form.Row>

                    <Form.Row label = 'Dodatan opis problema'>
                        <textarea type = 'text' disabled value = {this.state.additionalUserDescription}></textarea>
                    </Form.Row>

                    <Form.Row label = 'Klijent'>
                        <input type = 'text' disabled value = {this.state.client.name}/>
                        <input type = 'text' disabled value = {this.state.client.surname}/>
                        <input type = 'text' disabled value = {this.state.client.mail}/>
                        <input type = 'text' disabled value = {this.state.client.carModel}/>
                        <input type = 'text' disabled value = {this.state.client.number}/>
                        <input type = 'text' disabled value = {this.state.client.productionYear}/>
                        <input type = 'text' disabled value = {this.state.client.registration}/>
                    </Form.Row>

                    <Form.Row label = 'Serviser'>
                        <input type = 'text' disabled value = {this.state.servicer.name}/>
                        <input type = 'text' disabled value = {this.state.servicer.surname}/>
                        <input type = 'text' disabled value = {this.state.servicer.mail}/>
                        <input type = 'text' disabled value = {this.state.servicer.address}/>
                        <input type = 'text' disabled value = {this.state.servicer.number}/>
                    </Form.Row>

                    <Form.Row label = 'Komentari'>
                        {this.state.comments.map((comment) =>
                            <Form.Row label = {`${comment.author.name} ${comment.author.surname}: ${comment.createdAt}`}>
                                <textarea type = "text" disabled key = {comment.id} value = {comment.text}></textarea>
                            </Form.Row>
                        )}
                    </Form.Row>

                    <Form.Row>
                        <button className="btn1" onClick = {this.delete}> Izbriši termin </button>
                    </Form.Row>

                    <Form.Row>
                        <Link to='/pocetnaadmin'><Button className="btn1">Povratak na početnu</Button></Link>
                    </Form.Row>

                    <Form.Row>
                        <p><strong>{this.state.error}</strong></p>
                    </Form.Row>

                </Form>
            </Container>
        );
    }
}

export default SingleAppointment;