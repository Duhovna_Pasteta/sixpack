import React, {Component} from 'react';
import Form from '../../Form/Form';
import Button from '../../Button/Button';
import cookie from "react-cookies";
import Container from "../../Container/Container";
import {Link} from "react-router-dom";

class SingleAppointment extends Component{

    state = {
        data: [],
        carIssues: [],
        client: [],
        comments: [],
        appointmentDateTime: '',
        additionalUserDescription: '',
        comment: '',
        vehicleStatus: '',
        changeStatus: ''
    }

    componentWillMount(){
        if(cookie.load('role') === "ROLE_USER")
            this.props.history.push("/stranica_nije_pronadena");
    }

    onSubmit = (e) =>{
        e.preventDefault();

        if(this.state.comment === '')
            return;
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
            body: JSON.stringify({
                    text: this.state.comment,
                    appointmentId: this.props.match.params.id                }
            )
        };

        fetch('/api/comment', options)
            .then(response => response.json().then(json => {
                if(response.status === 400){
                    this.setState({error : 'Niste uspjeli dodati termin'});
                }else{
                    window.location.reload();
                }
            }));
    }


    componentDidMount()     {
        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
            }
        };

        fetch(`/api/appointment/${this.props.match.params.id}`, options)
            .then(data => data.json())
            .then(json => this.setState ( {carIssues: json.carIssues, client: json.client, comments: json.comments,
                                                            appointmentDateTime: json.appointmentDateTime, vehicleStatus: json.carStatus,
                                                            additionalUserDescription: json.additionalUserDescription, data:json}))
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    // changeStatus = () => {
    //     const options = {
    //         method: 'POST',
    //         headers: {
    //             'Authorization': 'Bearer ' + cookie.load('token')
    //         },
    //         body: JSON.stringify({
    //                 status: this.state.changeStatus
    //             }
    //
    //         )
    //     };
    //
    //     fetch(`/api/repairers/carStatus/${this.props.match.params.id}`, options)
    //         .then(response => {
    //             if (response.status === 400) {
    //                 this.setState({error: 'Status nije promijenjen'});
    //             } else {
    //                 window.location.reload();
    //                 alert('Status vozila je promijenjen');
    //             }});
    // }

    getPDF = () =>{
        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
            },
            responseType: 'blob'
        };

        fetch(`/api/repairers/pdf/${this.props.match.params.id}`, options)
            .then(response => {
                const filename =  response.headers.get('Content-Disposition').split('filename=')[1];
                response.blob().then(blob => {
                    let url = window.URL.createObjectURL(blob);
                    let a = document.createElement('a');
                    a.href = url;
                    a.download = filename;
                    a.click();
                });
            });
            // .then(response => {
            //     const file = new Blob(
            //         [response.data],
            //         {type: 'application/pdf'});
            //     const fileURL = URL.createObjectURL(file);
            //     window.open(fileURL);
            // });
        console.log(this.state);
    }

    changeCarStatus = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
        const options = {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token'),
                'Content-Type': 'text/plain'
            },
            body: event.target.value

        };
        fetch(`/api/repairers/carStatus/${this.props.match.params.id}`, options)
            .then(response => {
                if (response.status === 400) {
                    this.setState({error: 'Status nije promijenjen'});
                } else {
                    // window.location.reload();
                    alert('Status vozila je promijenjen');
                }});
    }


    delete = () => {
        const options = {
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
            }
        };

        fetch(`/api/appointment/${this.props.match.params.id}`, options)
            .then(response => {
                if (response.status === 400) {
                    this.setState({error: 'Termin nije obrisan'});
                } else {
                    alert('Termin je uspješno obrisan')
                    this.props.history.push('/pocetnaserviser')
            }});
    }

    render(){
        console.log(this.state);
        return(
            <Container title = 'Termin'>
                <Form onSubmit = {this.onSubmit}>
                    <Form.Row label = 'Vrijeme'>
                        <input type = 'text' disabled value = {this.state.appointmentDateTime}/>
                    </Form.Row>

                    <Form.Row label = 'Status'>
                        <select onChange = {this.changeCarStatus} name = 'changeStatus'>
                            <option selected={this.state.vehicleStatus === "Vozilo nije predano" ? "selected" : ""} value = "Vozilo nije predano" name = 'changeStatus'>Vozilo nije predano</option>
                            <option selected={this.state.vehicleStatus === "Vozilo je predano" ? "selected" : ""} value = "Vozilo je predano" name = 'changeStatus'>Vozilo je predano</option>
                            <option selected={this.state.vehicleStatus === "Vozilo je gotovo" ? "selected" : ""} value = "Vozilo je gotovo" name = 'changeStatus'>Vozilo je gotovo</option>
                        </select>
                    </Form.Row>

                    <Form.Row label = 'Problemi s autom'>
                        {this.state.carIssues.map((issue) => <input type = "text" disabled key = {issue.id} value = {issue.description}/>)}
                    </Form.Row>

                    <Form.Row label = 'Dodatan opis problema'>
                        <textarea type = 'text' disabled value = {this.state.additionalUserDescription}></textarea>
                    </Form.Row>

                    <Form.Row label = 'Klijent'>
                        <input type = 'text' disabled value = {this.state.client.name}/>
                        <input type = 'text' disabled value = {this.state.client.surname}/>
                        <input type = 'text' disabled value = {this.state.client.mail}/>
                        <input type = 'text' disabled value = {this.state.client.carModel}/>
                        <input type = 'text' disabled value = {this.state.client.number}/>
                        <input type = 'text' disabled value = {this.state.client.productionYear}/>
                        <input type = 'text' disabled value = {this.state.client.registration}/>
                    </Form.Row>

                    <Form.Row label = 'Komentari'>
                        {this.state.comments.map((comment) =>
                                <Form.Row label = {`${comment.author.name} ${comment.author.surname}: ${comment.createdAt}`}>
                                    <textarea type = "text" disabled key = {comment.id} value = {comment.text}></textarea>
                                </Form.Row>
                        )}
                    </Form.Row>

                    <Form.Row label = 'Unesite komentar'>
                        <textarea onChange = {this.handleChange} name = 'comment'></textarea>
                    </Form.Row>

                    <Form.Row>
                        <Button className="btn1" type = 'submit' >Dodaj komentar</Button>
                    </Form.Row>

                    {/*<Form.Row >*/}
                        {/*<Button className="btn1" onClick = {this.changeStatus}>Spremi status </Button>*/}
                    {/*</Form.Row>*/}

                    <Form.Row>
                        <button className="btn1" onClick = {this.delete}> Izbriši termin </button>
                    </Form.Row>

                    <Form.Row >
                        <Button className="btn1" onClick = {this.getPDF}>Dohvati PDF </Button>
                    </Form.Row>

                    <Form.Row>
                        <Link to='/pocetnaserviser'><Button className="btn1">Povratak</Button></Link>
                    </Form.Row>

                    <Form.Row>
                        <p><strong>{this.state.error}</strong></p>
                    </Form.Row>

                </Form>

            </Container>
        );
    }
}

export default SingleAppointment;