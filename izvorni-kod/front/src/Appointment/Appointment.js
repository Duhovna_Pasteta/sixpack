import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Appointment extends Component {

    render() {

        const {id, name, appointmentDateTime} = this.props.appointment;
        const body = `termin/${id}`;

        return (
            <Link to = {body}>
             <p>{id}. {name} {appointmentDateTime}</p>
            </Link>
        );
    }
}
export default Appointment;