import React, {Component} from 'react';
import "./ErrorPage.css"
import Container from "../Container/Container";

class ErrorPage extends Component{
    render(){
        return(
            <div className={"error404"}>
            <Container>

                <h1>Greška! <br/> Stranica nije pronađena<br/> ili niste ovlašteni za pristup</h1>

            </Container>
            </div>

        );
    }
}

export default ErrorPage;