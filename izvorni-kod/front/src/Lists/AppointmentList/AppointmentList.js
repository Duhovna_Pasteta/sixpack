import React, {Component} from 'react';
import Appointment2 from '../../Appointment/Appointment2'
import Container from "../../Container/Container";
import cookie from "react-cookies";
import {Link} from "react-router-dom";
import Button from "../../Button/Button";
import Form from "../../Form/Form";


class AppointmentList extends Component{
    state={
        appointments: []
    };

    componentWillMount(){
        if(cookie.load('role') !== "ROLE_ADMIN")
            this.props.history.push("/stranica_nije_pronadena");
    }

    componentDidMount() {

        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            }
    }

        fetch('api/appointment/reserved', options)
            .then(response => response.json())
            .then(json => this.setState ( {appointments: json}));
    }

    render() {
        console.log(this.state);
        return(
            <Container title='Lista termina'>
                <Form>
                {this.state.appointments.map(appointment =>
                        <Appointment2 key={appointment.id} appointment={appointment}/>)}
                <Form.Row>
                    <Link to='/pocetnaadmin'><Button className="btn1">Povratak</Button></Link>
                </Form.Row>
                </Form>
            </Container>
        );
    }
}

export default AppointmentList;