import React, {Component} from 'react';
import Servicer from '../../Servicer/Servicer';
import Container from "../../Container/Container";
import cookie from 'react-cookies';
import {Link} from "react-router-dom";
import Button from "../../Button/Button";
import Form from "../../Form/Form";

class ServicerList extends Component{
    state={
        servicers: [],
    };

    componentDidMount() {

        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
            }
        };
        fetch('/api/repairers', options)
          .then(data => data.json())
          .then(servicers => this.setState ( {servicers: servicers}))
    }

    componentWillMount(){
        if(cookie.load('role') !== "ROLE_ADMIN")
            this.props.history.push("/stranica_nije_pronadena");
    }

    render() {
            return(
                <Container title='Lista servisera'>
                    <Form>
                {
                    this.state.servicers.map(servicer =>
                        <Servicer key={servicer.id} servicer={servicer}/>
                    )
                }
                    <Form.Row>
                        <Link to='/pocetnaadmin'><Button className="btn1">Povratak</Button></Link>
                    </Form.Row>
                    </Form>
                </Container>
        );
    }
}
export default ServicerList;
