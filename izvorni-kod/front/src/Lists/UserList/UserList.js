import React, {Component} from 'react';
import Container from "../../Container/Container";
import User from "../../User/User";
import cookie from "react-cookies";
import Form from "../../Form/Form";
import {Link} from "react-router-dom";
import Button from "../../Button/Button";

class UserList extends Component{
    state={
        users: []
    };

    componentWillMount(){
        if(cookie.load('role') !== "ROLE_ADMIN")
            this.props.history.push("/stranica_nije_pronadena");
    }

    componentDidMount() {
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
        };
        fetch('/api/users/all', options)
            .then(response => response.json())
            .then(json => this.setState ( {users: json}))
    }

    render() {
        console.log(this.state);
        return(
            <Container title='Lista korisnika'>
                <Form>
                {
                    this.state.users.map(users =>
                        <User key={users.id} users={users}/>
                    )
                }
                <Form.Row>
                    <Link to='/pocetnaadmin'><Button className="btn1">Povratak</Button></Link>
                </Form.Row>
            </Form>
            </Container>
        );
    }
}

export default UserList;