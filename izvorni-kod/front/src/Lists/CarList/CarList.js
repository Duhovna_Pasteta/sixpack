import React, {Component} from 'react';
import Container from "../../Container/Container";
import cookie from 'react-cookies';
import {Link} from "react-router-dom";
import Button from "../../Button/Button";
import Form from "../../Form/Form";
import Car from "../../Servicer/Car/Car";

class CarList extends Component {
    state = {
        cars: [],
    };

    componentDidMount() {

        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            }
        };
        fetch('/api/cars/all', options)
            .then(response => response.json())
            .then(json => this.setState ( {cars: json}));
    }

    componentWillMount(){
        if(cookie.load('role') !== "ROLE_ADMIN")
            this.props.history.push("/stranica_nije_pronadena");
    }

    render() {
        return(
            <Container title='Zamjenska vozila'>
                <Form>
                    {this.state.cars.map(car =>
                            <Car key={car.id} car={car}/>)
                    }
                    <Form.Row>
                        <Link to='/pocetnaadmin'><Button className="btn1">Povratak</Button></Link>
                    </Form.Row>
                </Form>
            </Container>
        );
    }
}

export default CarList;