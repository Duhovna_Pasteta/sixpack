import React, { Component } from 'react';
import './App.css';
import AddServicer from "../Administrator/AddServicer/AddServicer";
import Registration from "../Registration/Registration"
import Login from "../Login/Login"
import {Switch, Route } from 'react-router-dom';
import Header from "../Header/Header";
import SingleServicer from "../Servicer/SingleServicer/SingleServicer";
import ServicerList from "../Lists/ServicerList/ServicerList";
import VehicleApplication from '../User/VehicleApplication/VehicleApplication';
import AdminHomepage from "../Administrator/AdminHomepage/AdminHomepage";
import AppointmentList from "../Lists/AppointmentList/AppointmentList";
import ServicerHomepage from "../Servicer/ServicerHomepage/ServicerHomepage";
import UserHomepage from "../User/UserHomepage/Userhomepage";
import Appointment from "../User/Appointment/Appointment";
import AutoservisInfo from "../Administrator/AutoservisInfo/AutoservisInfo";
import Link from "react-router-dom/es/Link";
import UserList from '../Lists/UserList/UserList';
import SingleUser from '../User/SingleUser/SingleUser';
import ErrorPage from '../ErrorPage/ErrorPage';
import About from "../About/About";
import Profile from "../Administrator/Profile/Profile";
import Term from  "../User/Term/Term";
import cookie from 'react-cookies';
import ForgottenPassword from '../ForgottenPassword/ForgottenPassword';
import SingleAppointment from '../Appointment/SingleAppointment/SingleAppointment';
import SingleAppointmentAdmin from '../Appointment/SingleAppointmentAdmin/SingleAppointmentAdmin';
import ChangePassword from '../ChangePassword/ChangePassword';
import CarList from "../Lists/CarList/CarList";
import SingleCar from "../Servicer/SingleCar/SingleCar";

class App extends Component {

    state = {
        loading: true,
        loggedIn: false,
    };

    componentDidMount() {
        this.setState({loggedIn: cookie.load('loggedIn')});
        this.setState({loading: false});
}

    onLogin = () => {
        this.setState( { loggedIn: true });
    };

    onLogout = () => {
        this.setState({loading: true});
        this.setState({ loggedIn: false });
        cookie.remove('loggedIn');
        cookie.remove('role');
        cookie.remove('id');
        cookie.remove('token');
        cookie.remove('tokenType');
        this.loadingFalse();
    };

    loadingFalse = () => {
        this.setState({loading: false});
    }

    render()
            {
                console.log(this.state);
        if (this.state.loading) {
            return <div/>
        }

        if(!this.state.loggedIn) {
            return (
                <div className='App'>
                    <header>
                        <ul>
                            <li>
                                <Link to="/prijava">PRIJAVA</Link>
                            </li>
                            <li className="onama">
                                <Link to='/registracija'>REGISTRACIJA</Link>
                            </li>
                            <li className="onama">
                                <Link to='/'>O NAMA</Link>
                            </li>
                        </ul>
                    </header>
                    <Switch>
                        <Route exact path='/prijava' render = {(props) => ( <Login onLogin = {this.onLogin}   {...props}/>)}/>
                        <Route exact path='/registracija' render = {(props) => (<Registration onLogin = {this.onLogin}  {...props}/>)}/>
                        <Route path='/' exact component={About}/>
                        <Route path='/zaboravilistezaporku' exact component={ForgottenPassword}/>
                        <Route path='*' exact component = {ErrorPage}/>
                    </Switch>
                </div>
            )
        }

         return (
            <div>
                <Header onLogout={this.onLogout}/>
                <div className="App">
                    <Switch>
                        <Route path='/' exact component={About}/>
                        <Route path='/dodajservisera' authorize = {['ROLE_ADMIN']} exact component={AddServicer}/>
                        <Route path='/serviser/:id' exact component={SingleServicer}/>
                        <Route path='/listaservisera' exact component = {ServicerList}/>
                        <Route path='/listatermina' exact component = {AppointmentList}/>
                        <Route path='/pocetnaklijent' exact component = {UserHomepage}/>
                        <Route path='/pocetnaserviser' exact component = {ServicerHomepage}/>
                        <Route path='/pocetnaadmin' exact component={AdminHomepage}/>
                        <Route path='/prijavavozila' exact component = {VehicleApplication}/>
                        <Route path='/odabirtermina' exact component = {Appointment}/>
                        <Route path='/onama' exact component = {AutoservisInfo}/>
                        <Route path='/listakorisnika' exact component = {UserList}/>
                        <Route path='/korisnik/:id' exact component = {SingleUser}/>
                        <Route path='/adminprofil' exact component = {Profile}/>
                        <Route path='/stanjevozila' exact component = {Term}/>
                        <Route path='/terminn/:id' exact component = {SingleAppointmentAdmin}/>
                        <Route path='/termin/:id' exact component = {SingleAppointment}/>
                        <Route path='/promjenasifre' exact component = {ChangePassword}/>
                        <Route path='/listaauta' exact component = {CarList}/>
                        <Route path='/auto/:id' exact component = {SingleCar}/>
                        <Route path='*' exact component = {ErrorPage}/>
                    </Switch>
                </div>
            </div>
        );
    }
}
export default App;