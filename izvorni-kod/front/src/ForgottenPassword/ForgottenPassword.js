import React, {Component} from 'react';
import Form from "../Form/Form.js";
import Container from "../Container/Container";
import Button from "../Button/Button";

class ForgottenPassword extends Component {

    state = {
        mail: '',
        info: '',
    };

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'text/plain'
            },
            body: this.state.mail

        };
        fetch('/api/password', options)
            .then(this.setState({info : 'Zaporka Vam je poslana na e-mail.'}))
        };

    render() {
        return (
            <Container title='Oporavak zaporke'>
                <Form onSubmit={this.onSubmit}>
                    <Form.Row label = "E-mail">
                        <input type="email" name = "mail" id = "korisničko ime" placeholder={"primjer@domena.hr"}
                               required onChange={this.handleChange} value={this.state.mail} />
                    </Form.Row>
                    <Button type="submit">U redu</Button>
                    <div>
                        <p><strong>{this.state.info}</strong></p>
                    </div>
                </Form>
            </Container>
        );
    }

}

export default ForgottenPassword;