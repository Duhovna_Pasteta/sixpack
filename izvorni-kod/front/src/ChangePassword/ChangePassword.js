import React, {Component} from 'react';
import Form from '../Form/Form';
import Container from "../Container/Container";
import cookie from "react-cookies";
import Button from '../Button/Button';

class ChangePassword extends Component{
    state = {
        oldPassword: '',
        newPassword: '',
        repeatedPassword: '',
        user: ''
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        console.log(this.state.newPassword);
        console.log(this.state.repeatedPassword);
        if(this.state.newPassword !== this.state.repeatedPassword){
            this.setState({error: 'Ponovljena zaporka nije ispravna'});
            return;
        }
        if(this.state.newPassword.length < 6){
            this.setState({error: 'Šifra mora imati minimalno 6 znakova'})
            return;
        }
        this.setState({error:''});
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
            body: JSON.stringify({
                    userId: cookie.load('id'),
                    oldPassword: this.state.oldPassword,
                    newPassword: this.state.newPassword
                }
            )
        };
        fetch('/api/password/change', options)
            .then(response => {
                if(response.status !== 200){
                    this.setState({error: 'Stara zaporka nije ispravna'});
                }
                else{
                    alert('Šifra je uspješno promijenjena')
                    if(cookie.load('role') === 'ROLE_USER')
                        this.props.history.push('/pocetnaklijent');
                    if(cookie.load('role') === 'ROLE_REPAIRER')
                        this.props.history.push('/pocetnaserviser');
                    if(cookie.load('role') === 'ROLE_ADMIN')
                        this.props.history.push('/pocetnaadmin')
                }
            })
    };

    render(){
        return(
            <Container title = 'Promijenite zaporku'>
                <Form onSubmit = {this.onSubmit}>
                    <Form.Row label = 'Unesite staru zaporku'>
                        <input type = 'password' onChange = {this.handleChange} name = 'oldPassword'/>
                    </Form.Row>

                    <Form.Row label = 'Unesite novu zaporuku'>
                        <input type = 'password' onChange = {this.handleChange} name = 'newPassword'/>
                    </Form.Row>

                    <Form.Row label = 'Ponovite novu zaporku'>
                        <input type = 'password' onChange = {this.handleChange} name = 'repeatedPassword' />
                    </Form.Row>

                    <Form.Row>
                        <Button type = 'submit'>Promijeni</Button>
                    </Form.Row>

                    <Form.Row>
                        <p><strong>{this.state.error}</strong></p>
                    </Form.Row>
                </Form>
            </Container>
        );
    }
}

export default ChangePassword;