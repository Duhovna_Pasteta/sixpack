import React from 'react';
import './Container.css';

const Container = ({ children, title }) => (
    <div className='Container'>
        {title && <h2>{title}</h2>}
        {children}
    </div>
);

export default Container;
