import React, {Component} from 'react';
import Form from "../../Form/Form.js";
import Container from "../../Container/Container";
import Button from "../../Button/Button";
import './AutoservisInfo.css'
import cookie from "react-cookies";
import {Link} from "react-router-dom";

class AutoservisInfo extends Component {

    state = {
        name: '',
        hq: '',
        address: '',
        number: '',
        id: '',
        oib: '',
        iban: '',
        about: []
    };

    onSubmit = (e) => {
        e.preventDefault();
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
            body: JSON.stringify({
                name: this.state.name,
                headquater: this.state.hq,
                iban: this.state.iban,
                firmId: this.state.id,
                location: this.state.address,
                phone: this.state.number,
                pin: this.state.oib
            })
        };
        fetch('/api/service-info/update', options)
            .then(response => {
                if (response.status === 401) {
                    this.setState({ error: response.message });
                } else {
                    this.props.history.push('/pocetnaadmin');
                    alert('Uspješno ste izmijenili podatke')
                }
            })
    };

    componentWillMount(){
        if(cookie.load('role') !== "ROLE_ADMIN")
            this.props.history.push("/stranica_nije_pronadena");
    }

    componentDidMount() {
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
        };
        fetch('/api/service-info/get', options)
            .then(data => data.json())
            .then(json => this.setState ( {name: json.name, hq: json.headquater, address:json.location, number: json.phone,
                                                            id: json.firmId, iban: json.iban, oib:json.pin, about:json}))
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        return (
            <Container title='Podaci o autoservisu'>
                <Form onSubmit={this.onSubmit}>
                    <table>
                        <tbody>
                        <tr>
                            <th>Naziv subjekta:</th>
                            <td><input  size="50" px type = "text" name = "name" onChange = {this.handleChange}
                                       value={this.state.name}/></td>
                        </tr>
                        <tr>
                            <th>Info telefon:</th>
                            <td><input size="50" px type = "text" name = "number" onChange = {this.handleChange}
                                       value={this.state.number}/></td>
                        </tr>
                        <tr>
                            <th>Matični broj:</th>
                            <td><input size="50" px type = "text" name = "id" onChange = {this.handleChange}
                                       value={this.state.id}/></td>
                        </tr>
                        <tr>
                            <th>OIB:</th>
                            <td><input size="50" px type = "text" name = "oib" onChange = {this.handleChange}
                                       value={this.state.oib}/></td>
                        </tr>
                        <tr>
                            <th>IBAN:</th>
                            <td><input size="50" px type = "text" name = "iban" onChange = {this.handleChange}
                                       value={this.state.iban}/></td>
                        </tr>
                        <tr>
                            <th>Sjedište:</th>
                            <td><input size="50" px type = "text" name = "hq" onChange = {this.handleChange}
                                       value={this.state.hq}/></td>
                        </tr>
                        <tr>
                            <th>Poslovnica:</th>
                            <td><input size="50" px type = "text" name = "address" onChange = {this.handleChange}
                                       value={this.state.address}/></td>
                        </tr>
                        </tbody>
                    </table>
                    <Form.Row>
                        <Button type="submit" class="btn1">Spremi izmjene</Button>
                    </Form.Row>
                    <Form.Row>
                        <Link to='/pocetnaadmin'><Button className="btn1">Povratak</Button></Link>
                    </Form.Row>
                </Form>
            </Container>
        );
    }
}

export default AutoservisInfo;