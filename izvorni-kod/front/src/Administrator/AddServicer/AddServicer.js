import React, {Component} from 'react';
import Form from '../../Form/Form.js';
import Container from "../../Container/Container";
import cookie from 'react-cookies';
import {Link} from "react-router-dom";
import Button from "../../Button/Button";

class AddServicer extends Component{

    state = {
        name: '',
        surname: '',
        password: '',
        mail: '',
        number:'',
        error: '',
        address: ''
    }

    componentWillMount(){
        if(cookie.load('role') !== "ROLE_ADMIN")
            this.props.history.push("/stranica_nije_pronadena");
    }

    handleChange = (event) =>{
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        const data = {
            name: this.state.name,
            surname: this.state.surname,
            mail: this.state.mail,
            password: this.state.password,
            number: this.state.number,
            address: this.state.address
        };
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
            body: JSON.stringify(data)
        };

        return fetch('/api/repairers/create', options)
            .then(response => {
                if (response.ok) {
                    this.props.history.push('/listaservisera');
                    alert('Uspješno ste dodali servisera');
                }
                else {
                    response.json().then(json => this.setState({error: json.message}))
                }
            })
    }


    render(){
        return(
            <Container>
                <Form onSubmit = {this.onSubmit}>
                    <Form.Row label = 'Ime'>
                        <input type = "text" name = "name" placeholder={"Ime"} required onChange={this.handleChange} value={this.state.value}/>
                    </Form.Row>

                    <Form.Row label = 'Prezime'>
                        <input type = "text" name = "surname" placeholder={"Prezime"} required onChange = {this.handleChange} value = {this.state.value}/>
                    </Form.Row>

                    <Form.Row label = 'E-mail'>
                        <input type = "email" name = "mail" placeholder={"E-mail"} required onChange={this.handleChange} value = {this.state.value}/>
                    </Form.Row>

                    <Form.Row label = 'Zaporka'>
                        <input type = "password" name = "password" placeholder={"Zaporka"} required onChange = {this.handleChange} value = {this.state.value}/>
                    </Form.Row>

                    <Form.Row label  = 'Kućna adresa'>
                        <input type = "text" name = "address" placeholder = {"Adresa"} required onChange = {this.handleChange} value = {this.state.address}/>
                    </Form.Row>

                    <Form.Row label = 'Telefonski broj'>
                        <input type = "text" name = "number" placeholder={"Telefonski broj"} required onChange={this.handleChange} value = {this.state.number}/>
                    </Form.Row>

                    <Form.Row>
                    <button type = 'submit' className="btn1">Dodaj servisera</button>
                    </Form.Row>

                    <Form.Row>
                        <Link to='/pocetnaadmin'><Button className="btn1">Odustani</Button></Link>
                    </Form.Row>
                    <p><strong>{this.state.error}</strong></p>
                </Form>
            </Container>
        );
    }
}

export default AddServicer;