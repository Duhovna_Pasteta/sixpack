import React, {Component} from 'react';
import Container from "../../Container/Container";
import Form from "../../Form/Form";
import Button from "../../Button/Button";
import cookie from "react-cookies";
import {Link} from "react-router-dom";

class Profile extends Component{

    state = {
        name: '',
        surname: '',
        mail: '',
        number: '',
        address: '',
        password: '',
        id: ''
    };

    componentWillMount(){
        if(cookie.load('role') !== "ROLE_ADMIN")
            this.props.history.push("/stranica_nije_pronadena");
    }

    componentDidMount() {
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
        };
        fetch('/api/users/me', options)
            .then(response => response.json())
            .then(json => this.setState ({name: json.name, surname: json.surname, mail: json.mail, number: json.number,
                address: json.address, password: json.password, id: json.id}))
    }

    onSubmit = (e) => {
        e.preventDefault();
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
            body: JSON.stringify({
                name: this.state.name,
                surname:this.state.surname,
                password: this.state.password,
                mail: this.state.mail,
                address: this.state.address,
                number: this.state.number
            })
        };
        return fetch(`/api/admin/edit/${this.state.id}`, options)
            .then(response => {
                if (response.ok) {
                    this.props.history.push('/pocetnaadmin');
                    alert('Uspješno ste izmijenili podatke')
                }
                else {
                    response.json().then(json => this.setState({error: json.message}))
                }
            })
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };



    render(){
        return(
            <Container title = "Moj profil">
                <Form onSubmit={this.onSubmit}>
                    <table>
                        <tbody>
                        <tr>
                            <th>Ime:</th>
                            <td><input size="50" px type = "text" name = "name" onChange = {this.handleChange}
                                       value={this.state.name} disabled/></td>
                        </tr>
                        <tr>
                            <th>Prezime:</th>
                            <td><input size="50" px type = "text" name = "surname" onChange = {this.handleChange}
                                       value={this.state.surname} disabled/></td>
                        </tr>
                        <tr>
                            <th>E-mail:</th>
                            <td><input size="50" px name = "mail" type="email" onChange = {this.handleChange}
                                       value={this.state.mail} disabled/></td>
                        </tr>
                        <tr>
                            <th>Telefonski broj:</th>
                            <td><input size="50" px type = "text" name = "number" onChange = {this.handleChange}
                                       value={this.state.number}/></td>
                        </tr>
                        <tr>
                            <th>Adresa:</th>
                            <td><input size="50" px type = "text" name = "address" onChange = {this.handleChange}
                                       value={this.state.address}/></td>
                        </tr>

                        </tbody>
                    </table>
                    <Form.Row>
                        <Button type="submit" class="btn1">Spremi izmjene</Button>
                    </Form.Row>

                    <Form.Row>
                        <Link to='/pocetnaadmin'><Button className="btn1">Povratak</Button></Link>
                    </Form.Row>

                    <Form.Row>
                        <p>{this.state.error}</p>
                    </Form.Row>
                </Form>
            </Container>
        );
    }
}

export default Profile;