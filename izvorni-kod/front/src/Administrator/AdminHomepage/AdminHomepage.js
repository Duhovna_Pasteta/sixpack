import React, {Component} from 'react';
import Form from "../../Form/Form.js";
import Container from "../../Container/Container";
import Button from "../../Button/Button";
import {Link} from "react-router-dom";
import cookie from "react-cookies";

class AdminHomepage extends Component {


    componentWillMount(){
        if(cookie.load('role') !== "ROLE_ADMIN")
            this.props.history.push("/stranica_nije_pronadena");
    }

    render() {
        return (
            <Container title='Odaberite opciju'>
                <Form onSubmit={this.onSubmit}>
                    <Form.Row >
                        <Link to='/listaservisera'><Button className="btn1">Lista servisera</Button></Link>
                    </Form.Row>

                    <Form.Row>
                        <Link to='/dodajservisera'><Button className="btn1">Dodavanje servisera</Button></Link>
                    </Form.Row>

                    <Form.Row>
                        <Link to='/listatermina'><Button className="btn1">Pregled termina</Button></Link>
                    </Form.Row>

                    <Form.Row>
                        <Link to = '/listakorisnika'> <Button className="btn1">Pregled korisnika</Button> </Link>
                    </Form.Row>

                    <Form.Row>
                        <Link to='/listaauta'><Button className="btn1">Zamjenska vozila</Button></Link>
                    </Form.Row>

                    <Form.Row>
                        <Link to = '/adminprofil'><Button className="btn1">Moj profil</Button> </Link>
                    </Form.Row>

                    <Form.Row>
                        <Link to='/promjenasifre'><Button className="btn1">Promijeni zaporku</Button></Link>
                    </Form.Row>

                    <Form.Row>
                        <Link to='/onama'><Button className="btn1">Kontakt podaci</Button></Link>
                    </Form.Row>

                </Form>
            </Container>
        );
    }
}

export default AdminHomepage;