import React, {Component} from 'react';
import Form from '../../Form/Form';
import Button from '../../Button/Button';
import cookie from "react-cookies";
import Container from "../../Container/Container";
import {Link} from "react-router-dom";

class SingleAppointment extends Component {

    state = {
        loading: true,
        data: [],
        status: '',
        exist: '',
        vehicleStatus: '',
        id: '',
        carIssues: [],
        servicer: [],
        comments: [],
        appointmentDateTime: '',
        additionalUserDescription: '',
        comment: '',
    }

    componentWillMount() {
        if (cookie.load('role') !== "ROLE_USER")
            this.props.history.push("/stranica_nije_pronadena");

        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
            }
        };

        fetch('/api/appointment/my', options)
            .then(response => {
                if (response.status === 200) {
                    this.setState({exist: 'exist'});
                    this.setState({loading: false});
                } else
                    this.setState({loading: false});
                    this.setState({data: response.status})
            });
    }

    onSubmit = (e) => {
        e.preventDefault();
        if (this.state.comment === '')
            return;
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
            body: JSON.stringify({
                    text: this.state.comment,
                    appointmentId: this.state.id
                }
            )
        };

        fetch('/api/comment', options)
            .then(response => response.json().then(json => {
                if (response.status === 400) {
                    this.setState({error: 'Niste uspjeli dodati komentar'});
                } else {
                    window.location.reload();
                }
            }));
    }


    componentDidMount() {
        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
            }
        };
        console.log(this.state.status);

        fetch("/api/appointment/my", options)
            .then(response => {
                if(response.status === 200) {
                    fetch(`/api/appointment/my`, options)
                        .then(response => response.json().then(json => {
                            if(response.status === 400){
                                this.setState({data : response});
                            }else{
                                this.setState({carIssues: json.carIssues, servicer: json.repairer, comments: json.comments,
                                    appointmentDateTime: json.appointmentDateTime, id: json.id, vehicleStatus: json.carStatus,
                                    additionalUserDescription: json.additionalUserDescription, data: json})
                            }
                        }))
                }
            })

    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    delete = () => {
        const options = {
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
            }
        };

        fetch(`/api/appointment/${this.state.id}`, options)
            .then(response => {
                if (response.status === 400) {
                    this.setState({error: 'Termin nije obrisan'});
                } else {
                    this.props.history.push('/pocetnaklijent')
                    alert('Uspješno ste obrisali termin')
                }
            });
    }

    render() {
        console.log(this.state);
        if (this.state.loading) {
            return <div/>
        }
        if (this.state.exist !== "exist") {
            return (
                <Container title='Termin'>
                    <Form>
                    <Form.Row>
                        <p><strong>Nemate zakazan termin.</strong></p>
                    </Form.Row>

                    <Form.Row>
                        <Link to='/pocetnaklijent'><Button className="btn1">Povratak</Button></Link>
                    </Form.Row>
                    </Form>

                </Container>
            );
        } else {
            return (
                <Container title='Termin'>
                    <Form onSubmit={this.onSubmit}>

                        <Form.Row label='Trenutno stanje'>
                            <input type="text" disabled value={this.state.vehicleStatus}/>
                        </Form.Row>
                        <Form.Row label='Vrijeme'>
                            <input type='text' disabled value={this.state.appointmentDateTime}/>
                        </Form.Row>

                        <Form.Row label='Problemi s autom'>
                            {this.state.carIssues.map((issue) => <input type="text" disabled key={issue.id}
                                                                        value={issue.description}/>)}
                        </Form.Row>

                        <Form.Row label='Dodatan opis problema'>
                            <textarea type='text' disabled value={this.state.additionalUserDescription}></textarea>
                        </Form.Row>

                        <Form.Row label='Serviser'>
                            <input type='text' disabled value={this.state.servicer.name}/>
                            <input type='text' disabled value={this.state.servicer.surname}/>
                            <input type='text' disabled value={this.state.servicer.mail}/>
                            <input type='text' disabled value={this.state.servicer.number}/>
                        </Form.Row>

                        <Form.Row label = 'Komentari'>
                            {this.state.comments.map((comment) =>
                                <Form.Row label = {`${comment.author.name} ${comment.author.surname}: ${comment.createdAt}`}>
                                    <textarea type = "text" disabled key = {comment.id} value = {comment.text}></textarea>
                                </Form.Row>
                            )}
                        </Form.Row>

                        <Form.Row label='Unesite komentar'>
                            <textarea onChange={this.handleChange} name='comment'></textarea>
                        </Form.Row>

                        <Form.Row>
                            <Button type='submit' className="btn1">Dodaj komentar</Button>
                        </Form.Row>

                        <Form.Row>
                            <Button className="btn1" onClick={this.delete}> Izbriši termin</Button>
                        </Form.Row>

                        <Form.Row>
                            <Link to='/pocetnaklijent'><Button className="btn1">Povratak</Button></Link>
                        </Form.Row>

                        <Form.Row>
                            <p><strong>{this.state.error}</strong></p>
                        </Form.Row>
                    </Form>
                </Container>
            );
        }
    }
}

export default SingleAppointment;