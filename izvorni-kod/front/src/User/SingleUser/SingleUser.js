import React, {Component} from 'react'
import Container from "../../Container/Container";
import Form from "../../Form/Form";
import Button from "../../Button/Button"
import cookie from "react-cookies";
import {Link} from "react-router-dom";

class SingleUser extends Component{

    state = {
        name: '',
        surname: '',
        mail: '',
        address: '',
        carModel: '',
        number: '',
        registration: '',
        productionYear: '',
        password: '',
        data:[]
    };

    componentWillMount(){
        if(cookie.load('role') !== "ROLE_ADMIN")
            this.props.history.push("/stranica_nije_pronadena");
    }

        componentDidMount() {
            // Šalje ID serveru i vraća podatke o serviseru
            const options = {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + cookie.load('token')
                },
            };
            fetch(`/api/users/${this.props.match.params.id}`, options)
                .then(response => response.json())
                .then(json => this.setState({name: json.name, surname:json.surname, mail: json.mail,
                                                            address: json.address, carModel: json.carModel, number: json.number,
                                                            registration: json.registration, productionYear: json.productionYear}));
        }

    onSubmit = (e) => {
        e.preventDefault();
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
            body: JSON.stringify({
                name: this.state.name,
                surname:this.state.surname,
                password: this.state.password,
                mail: this.state.mail,
                address: this.state.address,
                carModel: this.state.carModel,
                number: this.state.number,
                registration: this.state.registration,
                productionYear: this.state.productionYear
            })
        };
        return fetch(`/api/admin/edit/${this.props.match.params.id}`, options)
            .then(response => {
                if (response.ok) {
                    this.props.history.push('/listakorisnika');
                    alert('Uspješno ste izmijenili podatke');
                }
                else {
                    response.json().then(json => this.setState({error: json.message}))
                }
            })
    }

    // Promjene vrijednosti polja ovisno o tome da li je polje intput ili checkbox
    handleChange = (event) => {
        if(event.target.type === "checkbox"){
            event.target.value = event.target.checked;
            this.setState({
                [event.target.name]: event.target.value
            });
        }
        else{
            this.setState({
                [event.target.name]: event.target.value
            });
        };
    }


    render(){
        console.log(this.state);
        return (
            <Container title = "Podaci o korisniku">
                <Form onSubmit = {this.onSubmit}>
                    <Form.Row label = 'Ime'>
                        <input type = "text" name = "name" onChange = {this.handleChange}
                               value={this.state.name} disabled />
                    </Form.Row>
                    <Form.Row label = 'Prezime'>
                        <input type = "text" name = "surname" value={this.state.surname}
                               onChange={this.handleChange} disabled/>
                    </Form.Row>

                    <Form.Row label = 'Email'>
                        <input type = "text" name = "mail" value = {this.state.mail}
                               onChange = {this.handleChange} disabled/>
                    </Form.Row>

                    <Form.Row label = 'Telefonski broj'>
                        <input type = "text" name = "number" value = {this.state.number}
                               onChange = {this.handleChange}/>
                    </Form.Row>
                    <Form.Row label = 'Adresa'>
                        <input type = "text" name = "address" value = {this.state.address}
                               onChange = {this.handleChange}/>
                    </Form.Row>

                    <div style={{display: this.state.carModel === null ? "none" : "" }}>
                        <Form.Row label = 'Model automobila'>
                            <input type = "text" name = "carModel" value = {this.state.carModel}
                                   onChange = {this.handleChange} />
                        </Form.Row>
                        <Form.Row label = 'Registracija'>
                            <input type = "text" name = "registration" value = {this.state.registration}
                                   onChange = {this.handleChange} />
                        </Form.Row>
                        <Form.Row label = 'Godina proizvodnje'>
                            <input type = "text" name = "productionYear" value = {this.state.productionYear}
                                   onChange = {this.handleChange} />
                        </Form.Row>
                    </div>

                    <Form.Row>
                    <Button type = "submit" className="btn1">Spremi promjene</Button>
                    </Form.Row>

                    <Form.Row>
                        <Link to='/pocetnaadmin'><Button className="btn1">Povratak na početnu</Button></Link>
                    </Form.Row>

                    <Form.Row>
                        <p>{this.state.error}</p>
                    </Form.Row>
                </Form>
            </Container>
        );
    }
}

export default SingleUser;