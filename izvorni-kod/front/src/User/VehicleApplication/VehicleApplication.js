import React, {Component} from 'react';
import Form from '../../Form/Form';
import Button from '../../Button/Button';
import Container from "../../Container/Container";
import cookie from "react-cookies";
import {Link} from "react-router-dom";

class VehicleApplication extends Component{
    
    state={
        loading: true,
        vehicles: [],
        vehicle: '',
        servicers: [],
        servicer: '0',
        appointments: [],
        appointment: '',
        problems: [],
        problem : [],
        additionalProblem :'',
        zamjenskoVozilo: "false",
        izabraniProblemi: '',
        exist: '',
        data: '',
        error: '',

    };
    componentWillMount(){
        if(cookie.load('role') !== "ROLE_USER")
            this.props.history.push("/stranica_nije_pronadena");

        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
            }
        };
        fetch('/api/appointment/my', options)
            .then(response => {
                if(response.status === 200){
                    this.setState({exist: 'exist'});
                    this.setState({loading: false});
                }
                else
                    this.setState({loading: false});
                this.setState({data: response.status})
            });
    }



  componentDidMount() {
        
        const options = {
            method: 'GET',
            headers: {
                  'Authorization': 'Bearer ' + cookie.load('token')
              }
        };

        fetch('/api/repairers', options)
          .then(response => response.json())
          .then(json => this.setState({servicers: json}, () => {this.random()})
          );

        fetch('/api/issue', options)
            .then(response => response.json())
            .then(json => this.setState({problems:json}));

        fetch('/api/cars/available', options)
            .then(response => response.json())
            .then(json => this.setState({vehicles: json}));

        this.random();
    };

    random = () =>{
        if(this.state.servicers.length === 0)
            return;
        var objectServicer;
        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
            }
        };

        var randomId = Math.floor((Math.random() * this.state.servicers.length - 1) + 1);
        var object =  this.state.servicers[randomId];
        objectServicer = object.id;
        fetch(`/api/repairers/appointments/${objectServicer}`, options)
            .then(response => response.json())
            .then(json => this.setState({appointments: json}));
    }

    onSubmit = (e) => {
        e.preventDefault();
        if(this.state.appointment === ''){
            this.setState({error: 'Morate odabrati termin'});
            return;
        }
        if((this.state.problem.length === 0) && (this.state.additionalProblem.length === 0)){
            this.setState({error: 'Morate odabrati probleme ili opisati problem'});
            return;
        }
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
            body: JSON.stringify({
                    appointmentId: this.state.appointment,
                    issueIds: this.state.problem,
                    description: this.state.additionalProblem,
                    substituteCarId: this.state.vehicle
                }
            )
        };

        return fetch('api/appointment/book',options)
            .then(response => {
                if (response.ok) {
                    this.props.history.push('/pocetnaklijent')
                }
                else {
                    response.json().then(json => this.setState({error: json.message}))
                }
            })
    }

    locked = () => {
        if (this.state.zamjenskoVozilo === "false")
            return false;
        else return true;
    };

    getFreeAppointments = (event) => {
        var objectServicer = '';
        if (event.target.value === '0') {
            var randomId = Math.floor((Math.random() * this.state.servicers.length - 1) + 1);
            var object =  this.state.servicers[randomId];
            objectServicer = object.id;
            // console.log(object);
        } else {
            this.setState({
                [event.target.name]: event.target.value
            });
            [objectServicer] = event.target.value;
        }

        const options = {
            method: 'GET',
            headers: {
            'Authorization': 'Bearer ' + cookie.load('token')
            }
        };
         fetch(`/api/repairers/appointments/${objectServicer}`, options)
             .then(response => response.json())
             .then(json => this.setState({appointments: json}));
    }

    handleChangeCheckbox = (event) => {
        if (event.target.type === "checkbox") {
            event.target.value = event.target.checked;
            this.setState({
                [event.target.name]: event.target.value
            });
        }
    }

        handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });

    }

    handleChangeProblems = (event) =>{
        var index = '';
        var array = this.state.problem;
        this.setState({izabraniProblemi: ''});
        if(array.includes(event.target.value)){
            index = array.indexOf(event.target.value);
            array.splice(index,1);
            this.setState({problem: array})
        }
        else{
            array.push(event.target.value);
            this.setState({problem: array})
        }
        this.setState({izabraniProblemi: 'Izabrani problemi: '+array.join()})

    }


    render() {
        console.log(this.state);
        if (this.state.loading) {
            return <div/>
        }

        if(this.state.exist === 'exist'){
            return(
                <Container title = "Prijava vozila">
                    <Form.Row>
                        <p><strong>Već imate zakazan termin</strong></p>
                    </Form.Row>
                </Container>
            );
        }

        return(
            <Container title = "Prijava vozila na servis">
                <Form onSubmit = {this.onSubmit}>
                    <Form.Row label = "Izaberite servisera">
                        <select name = 'servicer' onChange={this.getFreeAppointments}>
                            <option value = '0'>Nasumičan serviser</option>
                            {this.state.servicers.map((servicer) => <option key={servicer.id} value={servicer.id} onChange={this.getFreeAppointments}>{servicer.name} {servicer.surname}</option>)}
                        </select>
                    </Form.Row>

                    <Form.Row label = "Izaberite termin">
                        <select name = "appointment" onClick={this.handleChange}>
                            <option> </option>
                            {this.state.appointments.map((appointment) => <option key={appointment.id} value={appointment.id} onChange = {this.handleChange}>{appointment.appointmentDateTime}</option>)}
                        </select>
                    </Form.Row>

                    <Form.Row label = "Izaberite probleme">
                        <select multiple name = 'problem' value = {this.state.value}>
                            <option selected = 'selected' disabled> </option>
                            {this.state.problems.map((issue) => <option key={issue.id} value={issue.id} onClick = {this.handleChangeProblems}>{issue.id}. {issue.description}</option>)}
                        </select>
                    </Form.Row>

                    <Form.Row>
                        <p>{this.state.izabraniProblemi}</p>
                    </Form.Row>

                    <Form.Row label = "Dodatan opis problema">
                        <textarea name = 'additionalProblem' onChange={this.handleChange}></textarea>
                    </Form.Row>

                    <Form.Row>
                        <input type="checkbox" name = "zamjenskoVozilo" onChange={this.handleChangeCheckbox}/> Želim zamjensko vozilo <br/>
                    </Form.Row>

                    <Form.Row label = "Izaberite zamjensko vozilo">
                        <select  name = "vehicle"  disabled = {!this.locked()} onChange={this.handleChange}>
                            <option> </option>
                            {this.state.vehicles.map((car) => <option key={car.id} value={car.id} onChange = {this.handleChange} name = 'vehicle'>{car.manufacturer} {car.model}</option>)}
                        </select>
                    </Form.Row>

                    <Form.Row>
                        <Button type = 'submit' className="btn1"> Prijavi </Button>
                    </Form.Row>

                    <Form.Row>
                        <Link to='/pocetnaklijent'><Button className="btn1">Odustani</Button></Link>
                    </Form.Row>
                    <p className='error'><strong>{this.state.error}</strong></p>
                </Form>
            </Container>

        );
    }
}

export default VehicleApplication;