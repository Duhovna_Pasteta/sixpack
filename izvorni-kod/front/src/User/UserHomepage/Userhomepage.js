import React, {Component} from 'react';
import Form from "../../Form/Form.js";
import Container from "../../Container/Container";
import Button from "../../Button/Button";
import {Link} from "react-router-dom";
import cookie from "react-cookies";

class UserHomepage extends Component {

    componentWillMount(){
        if(cookie.load('role') !== "ROLE_USER")
            this.props.history.push("/stranica_nije_pronadena");
    }

    render() {
        return (
            <Container title='Odaberite opciju'>
                <Form onSubmit={this.onSubmit}>
                    <Form.Row>
                        <Link to='/prijavavozila'><Button className="btn1">Prijava vozila na popravak</Button></Link>
                    </Form.Row>
                    <Form.Row>
                        <Link to='/stanjevozila'><Button className="btn1">Stanje predanog vozila</Button></Link>
                    </Form.Row>
                    <Form.Row>
                        <Link to='/promjenasifre'><Button className="btn1">Promjeni zaporku</Button></Link>
                    </Form.Row>
                </Form>
            </Container>
        );
    }
}

export default UserHomepage;