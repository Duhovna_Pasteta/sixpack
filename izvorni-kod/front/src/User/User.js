import React, {Component} from 'react';
import { Link } from "react-router-dom";

class User extends Component {

    render() {

        const {id, name, surname} = this.props.users;
        const body = `korisnik/${id}`;

        return (
            <Link to = {body}>
                <p>{name} {surname}</p>
            </Link>
        );
    }
}

export default User;