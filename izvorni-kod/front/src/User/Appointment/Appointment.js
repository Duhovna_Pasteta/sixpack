import React, {Component} from 'react';
import Form from "../../Form/Form.js";
import Container from "../../Container/Container";
import Button from "../../Button/Button";
import {Link} from "react-router-dom";

class Appointment extends Component {

    render() {
        return (
            <Container title='Odaberite termin'>
                <Form onSubmit={this.onSubmit}>
                    <Form.Row >
                        <select>
                            <option>

                            </option>
                        </select>
                    </Form.Row>
                    <Form.Row >
                        <Link to='/odabirtermina'><Button type="submit">Dalje</Button></Link>
                    </Form.Row>

                </Form>
            </Container>
        );
    }
}

export default Appointment;