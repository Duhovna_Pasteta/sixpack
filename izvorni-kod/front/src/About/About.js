import React, {Component} from 'react';
import Form from "../Form/Form.js";
import Container from "../Container/Container";

class About extends Component {

    state = {
        name: '',
        hq: '',
        address: '',
        number: '',
        id: '',
        oib: '',
        iban: '',
        workinghours: 'pon-pet: 7-18h',
        receiving: '7-10h',
        takeover: '14-18h',
        about:[],
    };

    componentDidMount() {
        fetch('/api/service-info/get')
            .then(data => data.json())
            .then(json => this.setState ( {name: json.name, hq: json.headquater, address:json.location, number: json.phone,
                id: json.firmId, iban: json.iban, oib:json.pin, about:json}))
    }

    render() {
        return (
            <Container title='Podaci o autoservisu'>
                <Form>
                    <table>
                        <tbody>
                        <tr>
                            <th>Naziv subjekta:</th>
                            <td>{this.state.name}</td>
                        </tr>
                        <tr>
                            <th>Info telefon:</th>
                            <td>{this.state.number}</td>
                        </tr>
                        <tr>
                            <th>Matični broj:</th>
                            <td>{this.state.id}</td>
                        </tr>
                        <tr>
                            <th>OIB:</th>
                            <td>{this.state.oib}</td>
                        </tr>
                        <tr>
                            <th>IBAN:</th>
                            <td>{this.state.iban}</td>
                        </tr>
                        <tr>
                            <th>Sjedište:</th>
                            <td>{this.state.hq}</td>
                        </tr>
                        <tr>
                            <th>Poslovnica:</th>
                            <td>{this.state.address}</td>
                        </tr>
                        <tr>
                        <th>Radno vrijeme:</th>
                         <td>{this.state.workinghours}</td>
                        </tr>
                         <tr>
                        <th>Prijem vozila:</th>
                        <td>{this.state.receiving}</td>
                        </tr>
                        <tr>
                        <th>Preuzimanje vozila:</th>
                        <td>{this.state.takeover}</td>
                        </tr>
                        </tbody>
                    </table>
                </Form>
            </Container>
        );
    }
}

export default About;