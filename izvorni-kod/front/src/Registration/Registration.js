import React, {Component} from 'react';
import './Registration.css';
import Form from "../Form/Form.js";
import Container from "../Container/Container";
import Button from "../Button/Button";

class Registration extends Component{

    state = {
        mail: '',
        password: '',
        name: '',
        surname: '',
        number: '',
        address: '',
        carModel: '',
        registration: '',
        productionYear: '',
        error: '',
        status: '',
        data: []
    };

    handleChange = (event) =>{
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    onSubmit = (e) => {
        e.preventDefault();
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                mail: this.state.mail,
                password: this.state.password,
                name: this.state.name,
                surname: this.state.surname,
                number: this.state.number,
                address: this.state.address,
                carModel: this.state.carModel,
                registration: this.state.registration,
                productionYear: this.state.productionYear
            })
        };
        fetch('/api/auth/register', options)
            .then(response => response.json().then(json => {
                if(response.status === 400){
                    this.setState({error : json.message});
                }else{
                    alert('Molimo vas da prije prijave potvrdite svoj mail');
                    this.props.history.push("/prijava");
                }
            }))
        // .then(response => response.json())
        // .then(json => this.setState ( {data: json}))
    };

    render(){
        console.log(this.state);
        return(
            <Container>
                <Form onSubmit={this.onSubmit}>
                        <Form.Row label = "Ime">
                            <input type="text" name = "name" placeholder={"Ime"} id = "name"
                                   required onChange={this.handleChange} value={this.state.name}/>
                        </Form.Row>

                        <Form.Row label = "Prezime">
                            <input type="text" name="surname" placeholder={"Prezime"} id="lastname"
                                   required onChange={this.handleChange} value={this.state.surname}/>
                        </Form.Row>

                        <Form.Row label = "E-mail">
                            <input type="email" name="mail" id="e_mail" placeholder="primjer@domena.hr"
                                   required onChange={this.handleChange} value={this.state.mail}/>
                        </Form.Row>

                        <Form.Row label = "Zaporka">
                            <input type = "password" name = "password" id = "zaporka" placeholder="Zaporka"
                                   required onChange={this.handleChange} value={this.state.password}/>
                        </Form.Row>

                        <Form.Row label = "Telefonski broj">
                            <input type = "text" name = "number" id = "telefonski broj" placeholder="Telefonski broj" maxLength={10} minLength={6}
                                   required onChange={this.handleChange} value={this.state.number}/>
                        </Form.Row>

                        <Form.Row label = "Kućna adresa">
                            <input type = "text" name = "address" id = "adress" placeholder = "Adresa"
                                   onChange = {this.handleChange} value = {this.state.address}/>
                        </Form.Row>

                        <Form.Row label = "Model automobila">
                            <select name = "carModel" id = "model_automobila"
                                    required onChange={this.handleChange} value={this.state.carModel}>
                                <option></option>
                                <option>M</option>
                                <option>I</option>
                                <option>LJ</option>
                                <option>E</option>
                                <option>N</option>
                                <option>K</option>
                                <option>O</option>
                            </select>
                        </Form.Row>

                        <Form.Row label = "Godina">
                            <input type="text" name="productionYear" id="godina" placeholder="npr. 2005" maxLength={4} minLength={4}
                                   required onChange={this.handleChange} value={this.state.productionYear}/>
                        </Form.Row>

                        <Form.Row label = "Registracija">
                            <input type="text" name="registration" id="registracija" placeholder="npr. ZG-123-AB"
                                   required onChange={this.handleChange} value={this.state.registration}  />
                        </Form.Row>
                        <Button type="submit">Registriraj se</Button>
                    <div>
                        <p className='greska'><strong>{this.state.error}</strong></p>
                    </div>
                </Form>
            </Container>
        );
    }
}

export default Registration;