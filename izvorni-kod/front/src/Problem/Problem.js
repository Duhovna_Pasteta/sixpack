import React, {Component} from 'react';

class Problem extends Component {

    render() {

        const {id, name,} = this.props.problem;

        return (
            <p>({id}. {name} )</p>
        );
    }
}

export default Problem;