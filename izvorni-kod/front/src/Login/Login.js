import React, {Component} from 'react';
import Button from "../Button/Button";
import {Link} from "react-router-dom";
import Form from "../Form/Form";
import './Login.css'
import Container from "../Container/Container";
import cookie from "react-cookies";

class Login extends Component{
    state = {
        mail: '',
        password: '',
        error: '',
    };

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                    mail: this.state.mail,
                    password: this.state.password
                }
            )
        };
        fetch('/api/auth/login', options)
            .then(response => response.json().then(json => {
                if(response.status === 401) {
                    this.setState({error: json.message});

                } else if (response.status === 400) {
                    this.setState({error: json.message});

                } else {
                    cookie.save('loggedIn', true);
                    cookie.save('role', json.role);
                    cookie.save('id', json.userId);
                    cookie.save('token', json.token);
                    cookie.save('tokenType', json.tokenType);
                    this.props.onLogin();
                    if(cookie.load('role') === 'ROLE_USER')
                        this.props.history.push('/pocetnaklijent');
                    if(cookie.load('role') === 'ROLE_REPAIRER')
                        this.props.history.push('/pocetnaserviser');
                    if(cookie.load('role') === 'ROLE_ADMIN')
                        this.props.history.push('/pocetnaadmin');

                }
            }))
    };

    render(){
        return(
            <Container title = ''>
                <Form onSubmit={this.onSubmit}>
                    <Form.Row label = "E-mail">
                        <input type="email" name = "mail" id = "korisničko ime" placeholder={"primjer@domena.hr"}
                               required onChange={this.handleChange} value={this.state.mail} />
                    </Form.Row>

                    <Form.Row label = "Zaporka">
                        <input type="password" name = "password" id = "zaporka" placeholder={"Zaporka"}
                               required onChange={this.handleChange} value={this.state.password}/>
                    </Form.Row>
                    <Button type="submit">Prijava</Button>
                    <Link to='/registracija'><Button>Registracija</Button></Link>
                    <Link to='/zaboravilistezaporku'><p className='forgot'>Zaboravili ste zaporku?</p></Link>
                    <div>
                        <p className='greska'><strong>{this.state.error}</strong></p>
                    </div>
                </Form>
            </Container>
        );
    }
}

export default Login;