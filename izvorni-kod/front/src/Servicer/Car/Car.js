import React, {Component} from 'react';
import { Link } from "react-router-dom";

class Car extends Component {

    render() {

        const {id, manufacturer, model, registeredBy} = this.props.car;
        const body = `auto/${id}`;

        return (
            <Link to = {body}>
                <p>({id}) {manufacturer} {model} ({registeredBy})</p>
            </Link>
        );
    }
}

export default Car;