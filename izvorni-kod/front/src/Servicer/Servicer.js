import React, {Component} from 'react';
import { Link } from "react-router-dom";

class Servicer extends Component {

    render() {

        const {id, name, surname} = this.props.servicer;
        const body = `serviser/${id}`;

        return (
            <Link to = {body}>
                <p>{name} {surname} ({id})</p>
            </Link>
        );
    }
}

export default Servicer;