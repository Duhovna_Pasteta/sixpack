import React, {Component} from 'react'
import Container from "../../Container/Container";
import Form from "../../Form/Form";
import Button from "../../Button/Button"
import cookie from "react-cookies";
import {Link} from "react-router-dom";

class SingleCar extends Component{

    state = {
        data: [],
        id: '',
        registeredBy: '',
        manufacturer: '',
        model: '',
        year: '',
        registration: '',
        registeredById: '',
        error: ''
    };

    componentWillMount(){
        if(cookie.load('role') !== "ROLE_ADMIN")
            this.props.history.push("/stranica_nije_pronadena");
    }

    componentDidMount() {
        // Šalje ID serveru i vraća podatke o zamjenskom vozilu
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
        };
        fetch(`/api/cars/one/${this.props.match.params.id}`, options)
            .then(response => response.json())
            .then(json => this.setState({id: json.id, registeredBy: json.registeredBy, model: json.model, manufacturer: json.manufacturer,
                registration: json.registration, year: json.year, registeredById: json.registeredById}));
    }

    lockIt = () => {
        if (this.state.registeredById == null)
            return true;
        else return false;
    };

    onSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + cookie.load('token')
                }
        };

        fetch(`/api/cars/free/${this.state.id}`, options)
        this.props.history.push('/listaauta');
        alert('Uspješno ste oslobodili automobil');
    }

    render(){
        // console.log(this.state);
        const body = `/korisnik/${this.state.registeredById}`;
        return (
            <Container title = "Podaci o zamjenskom voziluu">
                <Form onSubmit={this.onSubmit}>
                    <Form.Row label = "Korištenje">
                        <input type = "text" name = "name" value={this.state.registeredBy} disabled />
                    </Form.Row>

                    <Form.Row label = 'Proizvođač'>
                        <input type = "text" name = "manufacturer" value = {this.state.manufacturer} disabled/>
                    </Form.Row>

                    <Form.Row label = 'Model automobila'>
                        <input type = "text" name = "model" value = {this.state.model} disabled/>
                    </Form.Row>

                    <Form.Row label = 'Registracija'>
                        <input type = "text" name = "registration" value = {this.state.registration} disabled/>
                    </Form.Row>

                    <Form.Row label = 'Godina proizvodnje'>
                        <input type = "text" name = "year" value = {this.state.year} disabled/>
                    </Form.Row>

                    <Form.Row>
                        <Link to={body}><Button className="btn1"
                              disabled={this.lockIt()}>Pogledaj profil korisnika</Button></Link>
                    </Form.Row>

                   <Form.Row>
                       <Button type="submit" className="btn1">Oslobodi vozilo</Button>
                   </Form.Row>

                    <Form.Row>
                        <Link to='/pocetnaadmin'><Button className="btn1">Povratak na početnu</Button></Link>
                    </Form.Row>

                    <Form.Row>
                        <p><strong>{this.state.error}</strong></p>
                    </Form.Row>
                </Form>
            </Container>
        );
    }
}

export default SingleCar;