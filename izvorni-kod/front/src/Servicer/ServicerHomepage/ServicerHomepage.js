import React, {Component} from 'react';
import Container from "../../Container/Container";
import cookie from "react-cookies";
import Appointment from "../../Appointment/Appointment";
import Form from "../../Form/Form";
import {Link} from "react-router-dom";
import Button from "../../Button/Button";

class ServicerHomepage extends Component {

    state={
        appointments: [],
        error: ''
    };

    componentWillMount(){
        if(cookie.load('role') !== "ROLE_REPAIRER")
            this.props.history.push("/stranica_nije_pronadena");
    }

    componentDidMount() {
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookie.load('token')
            },
        };
        return fetch(`/api/appointment/repairer/${cookie.load('id')}`, options)
            .then(response => response.json().then(json => {
                if(response.status === 200){
                    this.setState({appointments: json});
                }else{
                    response.json().then(json => this.setState({error: json.message}))
                }
            }));
    }

    render() {
        console.log(this.state);
        return(
            <Container title='Nadolazeći termini'>
                {this.state.appointments.map(appointment =>
                    <Appointment key={appointment.id} appointment={appointment}/>)}

                <Form.Row>
                    <Link to='/promjenasifre'><Button className="btn1">Promjeni zaporku</Button></Link>
                </Form.Row>
            </Container>
        );
    }
}

export default ServicerHomepage;