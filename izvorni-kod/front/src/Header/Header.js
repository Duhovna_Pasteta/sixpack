import React, { Component } from 'react';
import { Link } from "react-router-dom";
import './Header.css';
import cookie from "react-cookies";

class Header extends Component {

    state = {
        link: "",
    };

    logout = () => {
            this.props.onLogout();
    };

    componentWillMount(){
        if(cookie.load('role') === "ROLE_ADMIN") {
            this.state.link = "/pocetnaadmin";
        } else if(cookie.load('role') === "ROLE_USER") {
            this.state.link = "/pocetnaklijent";
        } else if(cookie.load('role') === "ROLE_REPAIRER") {
            this.state.link = "/pocetnaserviser";
        }
    }

    render() {
        let hlp = this.state.link;
        return (
            <div className='Head'>
                <header>
                    <ul>
                        <li>
                            <Link to={hlp}>POČETNA</Link>
                        </li>
                        <li className="onama">
                            <Link to='/prijava' onClick={this.logout}>ODJAVA</Link>
                        </li>
                    </ul>
                </header>
            </div>
        )
    }
}

export default Header;