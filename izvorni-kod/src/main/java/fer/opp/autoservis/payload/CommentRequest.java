package fer.opp.autoservis.payload;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CommentRequest {

    @NotEmpty
    @NotNull
    private String text;

    @NotNull
    private Long appointmentId;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getAppointmentId() {
        return appointmentId;
    }

}
