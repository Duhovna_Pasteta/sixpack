package fer.opp.autoservis.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class EditRequest {

    @Size(max = 10)
    @NotBlank
    private String number;

    @Size(max = 100)
    private String address;

    private String carModel;

    private String registration;

    private String productionYear;

    public String getNumber() {
        return number;
    }

    public String getAddress() {
        return address;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getProductionYear() {
        return productionYear;
    }
}
