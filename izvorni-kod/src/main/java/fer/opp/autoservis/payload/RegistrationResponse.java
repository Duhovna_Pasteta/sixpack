package fer.opp.autoservis.payload;

public class RegistrationResponse {

    private Boolean success;

    private String message;

    private Long userId;

    private String role;

    public RegistrationResponse(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public RegistrationResponse(Boolean success, String message, String role, Long userId) {
        this.success = success;
        this.message = message;
        this.role = role;
        this.userId = userId;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public String getRole() {
        return role;
    }

    public Long getUserId() {
        return userId;
    }
}
