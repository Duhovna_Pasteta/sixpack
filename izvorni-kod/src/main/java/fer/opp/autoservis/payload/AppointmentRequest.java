package fer.opp.autoservis.payload;

import javax.validation.constraints.NotNull;

public class AppointmentRequest {

    @NotNull
    private Long appointmentId;

    private Long substituteCarId;

    private Long[] issueIds;

    private String description;

    public long getAppointmentId() {
        return appointmentId;
    }

    public Long getSubstituteCarId() {
        return substituteCarId;
    }

    public Long[] getIssueIds() {
        return issueIds;
    }

    public String getDescription() {
        return description;
    }
}
