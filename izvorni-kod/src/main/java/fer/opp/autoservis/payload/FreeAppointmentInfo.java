package fer.opp.autoservis.payload;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class FreeAppointmentInfo {

    @JsonFormat(pattern = "dd.MM.yyyy. HH:mm:ss")
    private LocalDateTime appointmentDateTime;

    private long id;

    public FreeAppointmentInfo(LocalDateTime appointmentDateTime, long id) {
        this.appointmentDateTime = appointmentDateTime;
        this.id = id;
    }

    public LocalDateTime getAppointmentDateTime() {
        return appointmentDateTime;
    }

    public long getId() {
        return id;
    }
}
