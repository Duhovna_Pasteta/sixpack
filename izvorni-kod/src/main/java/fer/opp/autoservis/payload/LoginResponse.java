package fer.opp.autoservis.payload;

public class LoginResponse {

    private String token;

    private String tokenType = "Bearer";

    private String role;

    private Long userId;

    public LoginResponse(String token, String role, Long userId) {
        this.token = token;
        this.role = role;
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public String getTokenType() {
        return tokenType;
    }

    public String getRole() {
        return role;
    }

    public Long getUserId() {
        return userId;
    }
}
