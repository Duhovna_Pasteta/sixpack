package fer.opp.autoservis.payload;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ChangePasswordRequest {

    @NotNull
    private long userId;

    @NotEmpty
    @Size(min = 6, max = 50)
    private String oldPassword;

    @NotEmpty
    @Size(min = 6, max = 50)
    private String newPassword;

    public long getUserId() {
        return userId;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }
}
