package fer.opp.autoservis.payload;

import com.fasterxml.jackson.annotation.JsonFormat;
import fer.opp.autoservis.domain.Appointment;

import java.time.LocalDateTime;

public class ReservedAppointmentInfo {

    @JsonFormat(pattern = "dd.MM.yyyy. HH:mm:ss")
    private LocalDateTime appointmentDateTime;

    private long id;

    private String name;

    public ReservedAppointmentInfo(Appointment appointment) {
        this.id = appointment.getId();
        this.appointmentDateTime = appointment.getAppointmentDateTime();
        this.name = appointment.getRepairer().getName() + " " + appointment.getRepairer().getSurname();
    }

    public ReservedAppointmentInfo(LocalDateTime appointmentDateTime, long id, String name) {
        this.appointmentDateTime = appointmentDateTime;
        this.id = id;
        this.name = name;
    }

    public LocalDateTime getAppointmentDateTime() {
        return appointmentDateTime;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

