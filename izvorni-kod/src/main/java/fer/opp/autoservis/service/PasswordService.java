package fer.opp.autoservis.service;

import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.payload.ChangePasswordRequest;

public interface PasswordService {

    void forgottenPassword(String mail);

    AppUser changePassword(ChangePasswordRequest request);
}
