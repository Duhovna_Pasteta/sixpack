package fer.opp.autoservis.service;

import fer.opp.autoservis.domain.AutoService;

public interface AutoServiceService {

    AutoService add(AutoService service);

    AutoService fetch();

    AutoService update(AutoService service);

}
