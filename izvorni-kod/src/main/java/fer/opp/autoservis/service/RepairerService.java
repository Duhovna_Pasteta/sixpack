package fer.opp.autoservis.service;

import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.Appointment;
import fer.opp.autoservis.payload.FreeAppointmentInfo;
import fer.opp.autoservis.payload.RegistrationRequest;
import fer.opp.autoservis.payload.RegistrationResponse;

import java.io.ByteArrayInputStream;
import java.util.List;

public interface RepairerService {

    RegistrationResponse createRepairer(RegistrationRequest repairer);

    List<AppUser> listAll();

    List<FreeAppointmentInfo> getFreeAppointmentTimes(long repairerId);

    ByteArrayInputStream appointmentConfirmation(long appointmentId);

    Appointment changeCarStatus(long appointmentId, String status);
}
