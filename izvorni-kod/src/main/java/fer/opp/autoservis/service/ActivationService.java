package fer.opp.autoservis.service;

public interface ActivationService {

    String activateUser(String value);
}
