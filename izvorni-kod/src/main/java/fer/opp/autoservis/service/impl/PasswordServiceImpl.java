package fer.opp.autoservis.service.impl;

import fer.opp.autoservis.dao.UserRepository;
import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.exception.BadRequestException;
import fer.opp.autoservis.payload.ChangePasswordRequest;
import fer.opp.autoservis.service.PasswordService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PasswordServiceImpl implements PasswordService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    @Lazy
    private JavaMailSender emailSender;

    @Autowired
    private PasswordEncoder encoder;

    @Override
    public void forgottenPassword(String mail) {
        Optional<AppUser> userOptional = userRepo.findByMail(mail);
        if(userOptional.isPresent()) {
            AppUser user = userOptional.get();

            Runnable sendMail = () -> {
                SimpleMailMessage message = new SimpleMailMessage();
                message.setTo(user.getMail());
                message.setSubject("Obnova lozinke");

                String newPassword = generateNewPassword();
                user.setPassword(encoder.encode(newPassword));
                userRepo.save(user);

                message.setText("Vaša nova lozinka je: " + newPassword);
                emailSender.send(message);
            };
            Thread thread = new Thread(sendMail);
            thread.start();
        }
    }

    private String generateNewPassword() {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        return RandomStringUtils.random(10, characters );
    }

    @Override
    public AppUser changePassword(ChangePasswordRequest request) {
        AppUser user = userRepo.findById(request.getUserId()).orElseThrow(() ->
                new BadRequestException("Korisnik s identifikacijskim brojem " + request.getUserId() + " ne postoji"));

        if(!encoder.matches(request.getOldPassword(), user.getPassword())) {
            throw new BadRequestException("Stara lozinka nije ispravna");
        }

        user.setPassword(encoder.encode(request.getNewPassword()));
        return userRepo.save(user);
    }
}
