package fer.opp.autoservis.service.impl;

import fer.opp.autoservis.dao.AppointmentRepository;
import fer.opp.autoservis.dao.IssueRepository;
import fer.opp.autoservis.dao.UserRepository;
import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.Appointment;
import fer.opp.autoservis.domain.CarIssue;
import fer.opp.autoservis.domain.RoleName;
import fer.opp.autoservis.exception.BadRequestException;
import fer.opp.autoservis.exception.RequestDeniedException;
import fer.opp.autoservis.payload.AppointmentRequest;
import fer.opp.autoservis.payload.AppointmentReservationInfo;
import fer.opp.autoservis.payload.ReservedAppointmentInfo;
import fer.opp.autoservis.security.UserDetailsImpl;
import fer.opp.autoservis.service.AppointmentService;
import fer.opp.autoservis.service.SubstituteCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    private AppointmentRepository appointmentRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private IssueRepository issueRepo;

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private SubstituteCarService carService;

    @Override
    public Appointment bookAppointment(AppointmentRequest request) {
        Appointment appointment = appointmentRepo.findById(request.getAppointmentId()).orElseThrow(() ->
                new BadRequestException("Termin s identifikacijskimm brojem: " + request.getAppointmentId() + " ne postoji"));

        if(!appointment.isFree()) {
            throw new BadRequestException("Termin je već rezerviran");
        }

        AppUser currentUser = getCurrentUser();

        long appointmentNum = appointmentRepo.findAllByClient(currentUser)
                .stream()
                .filter(a -> a.getAppointmentDateTime().isAfter(LocalDateTime.now()))
                .count();

        if(appointmentNum > 0) {
            throw new RequestDeniedException("Korisnik: " + currentUser.getMail() + " već ima rezerviran termin");
        }

        appointment.setClient(currentUser);
        appointment.setFree(false);
        appointment.setAdditionalUserDescription(request.getDescription());
        addIssues(appointment, request.getIssueIds());
        appointment.setReservationTime(LocalDateTime.now());

        if(request.getSubstituteCarId() != null) {
            carService.reserve(request.getSubstituteCarId());
        }

        appointmentRepo.save(appointment);
        sendReservationMail(appointment);

        return appointment;
    }

    @Override
    public Appointment fetch(long id) {
        Appointment appointment = appointmentRepo.findById(id).orElseThrow(() ->
                new BadRequestException("Termin s identifikacijskimm brojem: " + id + " ne postoji"));

        AppUser currentUser = getCurrentUser();

        if(!appointment.isFree()) {
            if (!currentUser.getRole().getName().equals(RoleName.ROLE_ADMIN) && !(currentUser.getId().equals(appointment.getClient().getId()) || currentUser.getId().equals(appointment.getRepairer().getId()))) {
                throw new BadRequestException("Ne možete gledati tuđe termine");
            }
        }

        return  appointment;
    }

    @Override
    public List<ReservedAppointmentInfo> fetchRepairersReservedAppointments(long repairerId) {
        AppUser repairer = userRepo.findById(repairerId).orElseThrow(() ->
                new BadRequestException("Serviser s identifikacijskom oznakom: " + repairerId + " ne postoji"));

        if(!repairer.getRole().getName().equals(RoleName.ROLE_REPAIRER)) {
            throw new BadRequestException("Serviser s identifikacijskom oznakom: " + repairerId + " ne postoji");
        }

        List<Appointment> appointments = appointmentRepo.findAllByFreeAndRepairer(false, repairer);
        List<ReservedAppointmentInfo> reservationTimes = new LinkedList<>();

        for(Appointment appointment : appointments) {
            reservationTimes.add(new ReservedAppointmentInfo(appointment.getAppointmentDateTime(), appointment.getId(),
                    appointment.getClient().getName() + " " + appointment.getClient().getSurname()));
        }

        return reservationTimes;
    }

    @Override
    public List<ReservedAppointmentInfo> getAllReservedAppointments() {
        List<Appointment> appointments = appointmentRepo.findAllByFree(false);
        List<ReservedAppointmentInfo> reservedInfo = new LinkedList<>();

        for(Appointment appointment : appointments) {
            reservedInfo.add(new ReservedAppointmentInfo(appointment));
        }

        return  reservedInfo;
    }

    @Override
    public void deleteAppointment(Long appointmentId) {
        Appointment appointment = appointmentRepo.findById(appointmentId).orElseThrow(() ->
                new BadRequestException("Termin s identifikacijskimm brojem: " + appointmentId + " ne postoji"));

        if(appointment.isFree()) {
            throw new BadRequestException("Ne možete obrisati prazan termin");
        }

        AppUser currentUser = getCurrentUser();

        if (!currentUser.getRole().getName().equals(RoleName.ROLE_ADMIN) && !(currentUser.getId().equals(appointment.getClient().getId()) || currentUser.getId().equals(appointment.getRepairer().getId()))) {
            throw new RequestDeniedException("Ne možete brisati tuđe termine");
        }

        appointment.setClient(null);
        appointment.setCarStatus("Vozilo nije predano");
        appointment.setFree(true);
        appointment.setAdditionalUserDescription(null);
        appointment.setReservationTime(null);
        appointment.setComments(new LinkedList<>());
        appointment.setCarIssues(new LinkedList<>());
        appointmentRepo.save(appointment);
    }

    @Override
    public Appointment getMyAppointment() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String mail = ((UserDetailsImpl)principal).getUsername();
        AppUser currentUser = userRepo.findByMail(mail).orElseThrow(() ->
                new BadRequestException("Korisnik s emailom: " + mail + " ne postoji"));

        Optional<Appointment> appointment = appointmentRepo.findAllByClient(currentUser)
                .stream().max(Comparator.comparing(Appointment::getAppointmentDateTime));

        return appointment.orElse(null);
    }

//############################################################################################## Private

    private AppUser getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String mail = ((UserDetailsImpl)principal).getUsername();
        return userRepo.findByMail(mail).orElseThrow(() -> new RequestDeniedException("Korisnik nije prijavljen"));
    }

    private void sendReservationMail(Appointment appointment) {
        Runnable sendMail = () -> {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(appointment.getClient().getMail());
            message.setSubject("Rezerviranje termina");
            LocalDateTime hlp = appointment.getAppointmentDateTime();
            StringBuilder text = new StringBuilder("Poštovani,\nuspješno ste rezervirali termin za predaju automobila na popravak koji će se održati "
                    + String.format("%d.%d.%d.", hlp.getDayOfMonth(), hlp.getMonthValue(), hlp.getYear()) + " u "
                    + String.format("0%d:%d.\n", hlp.getHour(), hlp.getMinute()));
            text.append("Serviser kojega ste odabrali je: ").append(appointment.getRepairer().getName()).append(" ").append(appointment.getRepairer().getSurname()).append(".\n");
            text.append("Problemi koje ste označili su: ");
            for(int i = 0; i < appointment.getCarIssues().size(); i++) {
                text.append("''").append(appointment.getCarIssues().get(i)).append("''");
                if(appointment.getCarIssues().size() != 1 && i != appointment.getCarIssues().size() - 1) {
                    text.append(",");
                }
                text.append(" ");
            }
            text.append(" s dodatnim opisom: ''").append(appointment.getAdditionalUserDescription()).append("''.\n\n");
            text.append("Lijep pozdrav,\nVaš AutoServis Najbolji Mehaničar.");

            message.setText(text.toString());
            emailSender.send(message);
        };
        Thread thread = new Thread(sendMail);
        thread.start();
    }

    private void addIssues(Appointment appointment, Long[] issueIds) {
        for(Long id : issueIds) {
            CarIssue issue = issueRepo.findById(id).orElseThrow(() ->
                    new BadRequestException("Predefinirani problem s identifikacijskimm brojem: " + id + " ne postoji"));
            appointment.addIssue(issue);
        }
    }
}
