package fer.opp.autoservis.service;

import fer.opp.autoservis.domain.Comment;
import fer.opp.autoservis.payload.CommentRequest;

public interface CommentService {

    Comment addComment(CommentRequest request);
}
