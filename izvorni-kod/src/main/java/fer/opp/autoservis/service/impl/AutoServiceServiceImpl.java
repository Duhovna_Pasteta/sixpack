package fer.opp.autoservis.service.impl;

import fer.opp.autoservis.dao.AutoServiceRepository;
import fer.opp.autoservis.domain.AutoService;
import fer.opp.autoservis.exception.RequestDeniedException;
import fer.opp.autoservis.exception.ResourceNotFoundException;
import fer.opp.autoservis.service.AutoServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AutoServiceServiceImpl implements AutoServiceService {

    @Autowired
    private AutoServiceRepository repo;

    @Override
    public AutoService add(AutoService service) {
        List<AutoService> list = repo.findAll();
        if (!list.isEmpty()) {
            throw new RequestDeniedException("AutoService information object already exists in database!"
                + " You can only edit it now using \"/update\".");
        }
        repo.save(service);
        return service;
    }

    @Override
    public AutoService fetch() {
        List<AutoService> list = repo.findAll();
        if (list.isEmpty()) {
            throw new ResourceNotFoundException("AutoService information object doesn't exit in the database."
                + " You can add it with \"/add\".");
        }
        return list.get(0);
    }

    @Override
    public AutoService update(AutoService service) {
        List<AutoService> list = repo.findAll();
        if (list.isEmpty()) {
            throw new ResourceNotFoundException("AutoService information object doesn't exit in the database."
                    + " You can add it with \"/add\".");
        }
        service.setId(list.get(0).getId());
        repo.save(service);
        return service;
    }
}
