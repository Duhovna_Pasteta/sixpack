package fer.opp.autoservis.service;

import fer.opp.autoservis.domain.CarIssue;

import java.util.List;

public interface IssueService {

    List<CarIssue> listAll();

    CarIssue create(String description);
}
