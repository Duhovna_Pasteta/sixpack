package fer.opp.autoservis.service.impl;

import fer.opp.autoservis.dao.PendingRegistrationRepository;
import fer.opp.autoservis.dao.UserRepository;
import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.PendingRegistration;
import fer.opp.autoservis.service.ActivationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ActivationServiceImpl implements ActivationService {

    @Autowired
    private PendingRegistrationRepository pendingRepo;

    @Autowired
    private UserRepository userRepo;

    @Value("${login_redirect_url}")
    private String goodRedirect;

    @Value("${bad_activation_value_url}")
    private String badRedirect;


    @Override
    public String activateUser(String value) {
        Optional<PendingRegistration> pending = pendingRepo.findByActivationValue(value);
        if(pending.isPresent()) {
            userRepo.save(new AppUser(pending.get()));
            pendingRepo.delete(pending.get());
            return goodRedirect;

        } else {
            return badRedirect;
        }
    }
}
