package fer.opp.autoservis.service.impl;

import fer.opp.autoservis.security.UserDetailsImpl;
import fer.opp.autoservis.util.PdfUtil;
import fer.opp.autoservis.dao.AppointmentRepository;
import fer.opp.autoservis.dao.UserRepository;
import fer.opp.autoservis.dao.RoleRepository;
import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.Appointment;
import fer.opp.autoservis.domain.Role;
import fer.opp.autoservis.domain.RoleName;
import fer.opp.autoservis.exception.AppException;
import fer.opp.autoservis.exception.BadRequestException;
import fer.opp.autoservis.payload.FreeAppointmentInfo;
import fer.opp.autoservis.payload.RegistrationRequest;
import fer.opp.autoservis.payload.RegistrationResponse;
import fer.opp.autoservis.service.RepairerService;
import fer.opp.autoservis.util.RepairerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
public class RepairerServiceImpl implements RepairerService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private AppointmentRepository appointmentRepo;

    @Override
    public RegistrationResponse createRepairer(RegistrationRequest request) {
        if(userRepo.existsByMail(request.getMail())) {
            return new RegistrationResponse(false, "Email adresa je već zauzeta!");
        }

        AppUser repairer = new AppUser(request.getMail(), request.getPassword(), request.getName(),
                request.getSurname(), request.getNumber(), request.getAddress());

        repairer.setPassword(encoder.encode(repairer.getPassword()));
        Role role = roleRepo.findByName(RoleName.ROLE_REPAIRER).
                orElseThrow(() -> new AppException("Uloga nije postavljena."));
        repairer.setRole(role);

        AppUser result = userRepo.save(repairer);

        createAppointments(repairer);

        return new RegistrationResponse(true, "Uspješno registriran serviser", "ROLE_REPAIRER", result.getId());
    }

    private void createAppointments(AppUser repairer) {
        LocalDateTime date = LocalDateTime.now();
        int maxDay = date.getHour() < 5 ? 9 : 10;

        for(int day = 1; day <= maxDay; day ++) {
            LocalDateTime next = date.plusDays(day);
            if(!next.getDayOfWeek().equals(DayOfWeek.SATURDAY)  && !next.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
                RepairerUtil.createAppointments(appointmentRepo, repairer, next);
            }
        }
    }

    @Override
    public List<AppUser> listAll() {
        Role repairerRole = roleRepo.findByName(RoleName.ROLE_REPAIRER)
                .orElseThrow(() -> new AppException("Uloga ne postoji"));

        return userRepo.findAllByRole(repairerRole);
    }

    @Override
    public List<FreeAppointmentInfo> getFreeAppointmentTimes(long repairerId) {
        AppUser repairer = userRepo.findById(repairerId).orElseThrow(() ->
                new BadRequestException("Serviser s identifikacijskom oznakom: " + repairerId + " ne postoji"));

        List<Appointment> appointments = appointmentRepo.findAllByFreeAndRepairer(true, repairer);
        List<FreeAppointmentInfo> freeInfo = new LinkedList<>();

        for(Appointment appointment : appointments) {
            freeInfo.add(new FreeAppointmentInfo(appointment.getAppointmentDateTime(), appointment.getId()));
        }

        return freeInfo;
    }

    @Override
    public ByteArrayInputStream appointmentConfirmation(long appointmentId) {
        Appointment appointment = appointmentRepo.findById(appointmentId).orElseThrow(() ->
                new BadRequestException("Termin s identifikacijskom oznakom: " + appointmentId + " ne postoji"));

        return PdfUtil.appointmentConfirmation(appointment);
    }

    @Override
    public Appointment changeCarStatus(long appointmentId, String status) {
        Appointment appointment = appointmentRepo.findById(appointmentId).orElseThrow(() ->
                new BadRequestException("Termin s identifikacijskim brojem: " + appointmentId + " ne postoji"));

        if(appointment.isFree()) {
            throw new BadRequestException("Ne možete mijenjati status slobodnog termina");
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String mail = ((UserDetailsImpl)principal).getUsername();

        if(!appointment.getRepairer().getMail().equals(mail)) {
            throw new BadRequestException("Ne možete mijenjti status tuđih termina");
        }

        if(!(status.equals("Vozilo nije predano")  || status.equals("Vozilo je predano") || status.equals("Vozilo je gotovo"))) {
            throw new BadRequestException("Ne valjali status vozila: " + status);
        }

        appointment.setCarStatus(status);
        return appointmentRepo.save(appointment);
    }

}
