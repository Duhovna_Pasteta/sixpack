package fer.opp.autoservis.service;

import fer.opp.autoservis.domain.AppUser;

import java.util.List;

public interface UserService {

    AppUser one(Long id);

    List<AppUser> listAll();

    AppUser myProfile();

}
