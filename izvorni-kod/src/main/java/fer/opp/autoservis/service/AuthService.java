package fer.opp.autoservis.service;

import fer.opp.autoservis.payload.LoginRequest;
import fer.opp.autoservis.payload.LoginResponse;
import fer.opp.autoservis.payload.RegistrationRequest;
import fer.opp.autoservis.payload.RegistrationResponse;

public interface AuthService {

    LoginResponse login(LoginRequest request);

    RegistrationResponse register(RegistrationRequest request);
}
