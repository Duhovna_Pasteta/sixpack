package fer.opp.autoservis.service.impl;

import fer.opp.autoservis.dao.AppointmentRepository;
import fer.opp.autoservis.dao.CommentRepository;
import fer.opp.autoservis.dao.UserRepository;
import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.Appointment;
import fer.opp.autoservis.domain.Comment;
import fer.opp.autoservis.exception.BadRequestException;
import fer.opp.autoservis.exception.RequestDeniedException;
import fer.opp.autoservis.payload.CommentRequest;
import fer.opp.autoservis.security.UserDetailsImpl;
import fer.opp.autoservis.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private AppointmentRepository appointmentRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private CommentRepository commentRepo;

    @Override
    public Comment addComment(CommentRequest request) {
        Appointment appointment = appointmentRepo.findById(request.getAppointmentId()).orElseThrow(() ->
                new BadRequestException("Termin s identifikacijskimm brojem: " + request.getAppointmentId() + " ne postoji"));

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String mail = ((UserDetailsImpl)principal).getUsername();
        AppUser currentUser = userRepo.findByMail(mail).orElseThrow(() ->
                new RequestDeniedException("Korisnik nije prijavljen"));

        if(!(currentUser.getId().equals(appointment.getClient().getId()) || currentUser.getId().equals(appointment.getRepairer().getId()))) {
            throw new BadRequestException("Ne možete komentirati tuđe termine");
        }

        Comment comment = new Comment(currentUser, request.getText());
        appointment.addComment(comment);

        return commentRepo.save(comment);
    }
}
