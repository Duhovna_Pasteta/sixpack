package fer.opp.autoservis.service.impl;

import fer.opp.autoservis.dao.PendingRegistrationRepository;
import fer.opp.autoservis.domain.PendingRegistration;
import fer.opp.autoservis.util.ActivationUtil;
import fer.opp.autoservis.util.ValidationUtil;
import fer.opp.autoservis.dao.RoleRepository;
import fer.opp.autoservis.dao.UserRepository;
import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.Role;
import fer.opp.autoservis.domain.RoleName;
import fer.opp.autoservis.exception.AppException;
import fer.opp.autoservis.exception.BadRequestException;
import fer.opp.autoservis.payload.LoginResponse;
import fer.opp.autoservis.payload.LoginRequest;
import fer.opp.autoservis.payload.RegistrationResponse;
import fer.opp.autoservis.payload.RegistrationRequest;
import fer.opp.autoservis.security.JwtTokenProvider;
import fer.opp.autoservis.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private AuthenticationManager authManager;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private PendingRegistrationRepository pendingRepo;

    @Autowired
    @Lazy
    private JavaMailSender emailSender;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Override
    public LoginResponse login(LoginRequest request) {
        if(pendingRepo.existsByMail(request.getMail())) {
            throw new BadRequestException("Niste potvrdili svoj korisnički račun");
        }

        AppUser user = userRepo.findByMail(request.getMail()).orElseThrow(() ->
                new BadRequestException("Netočna lozinka ili nepostojeće korisničko ime"));

        if(!encoder.matches(request.getPassword(), user.getPassword())) {
            throw new BadRequestException("Netočna lozinka ili nepostojeće korisničko ime");
        }

        Authentication authentication = authManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getMail(),
                        request.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return new LoginResponse(jwt, user.getRole().toString(), user.getId());
    }

    @Override
    public RegistrationResponse register(RegistrationRequest request) {
        if(userRepo.existsByMail(request.getMail())) {
            return new RegistrationResponse(false, "Email adresa je već zauzeta!");
        }

        request.setRegistration(request.getRegistration().toUpperCase());
        if(userRepo.existsByRegistration(request.getRegistration())) {
            return new RegistrationResponse(false, "Registracija automobila je već zauzeta!");
        }

        ValidationUtil.validateTelephoneNumber(request.getNumber());
        ValidationUtil.validateCarInfo(request.getCarModel(), request.getRegistration(), request.getProductionYear());

        PendingRegistration pending = new PendingRegistration(request);

        pending.setActivationValue(ActivationUtil.generateActivationValue());
        ActivationUtil.sendActivationMail(pending.getMail(), pending.getActivationValue(), emailSender);

        pending.setPassword(encoder.encode(pending.getPassword()));
        Role userRole = roleRepo.findByName(RoleName.ROLE_USER).orElseThrow(() -> new AppException("Uloga nije postaljvena."));
        pending.setRole(userRole);

        pendingRepo.save(pending);
        return new RegistrationResponse(true, "Registracija uspješna. Molimo potvrdite svoju email adresu.");
    }

}
