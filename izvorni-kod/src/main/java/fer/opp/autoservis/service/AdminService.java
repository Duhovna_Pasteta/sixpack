package fer.opp.autoservis.service;

import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.payload.EditRequest;

public interface AdminService {

    AppUser editUser(long id, EditRequest request);
}
