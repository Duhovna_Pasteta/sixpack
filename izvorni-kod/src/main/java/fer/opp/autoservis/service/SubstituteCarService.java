package fer.opp.autoservis.service;

import fer.opp.autoservis.domain.SubstituteCar;

import java.util.List;

public interface SubstituteCarService {

    SubstituteCar fetchMy();

    List<SubstituteCar> fetchAvailableCars();

    List<SubstituteCar> fetchAll();

    SubstituteCar reserve(Long carId);

    SubstituteCar free(long carId);

    SubstituteCar add(SubstituteCar car);

    SubstituteCar fetchOne(long carId);
}
