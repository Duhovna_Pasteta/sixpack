package fer.opp.autoservis.service.impl;

import fer.opp.autoservis.payload.EditRequest;
import fer.opp.autoservis.util.ValidationUtil;
import fer.opp.autoservis.dao.UserRepository;
import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.RoleName;
import fer.opp.autoservis.exception.BadRequestException;
import fer.opp.autoservis.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private PasswordEncoder encoder;

    @Override
    public AppUser editUser(long id, EditRequest request) {
        AppUser user = userRepo.findById(id).orElseThrow(() ->
                new BadRequestException("Korisnik s identifikacijskimm brojem: " + id + " ne postoji"));

//        if(!user.getMail().equals(request.getMail())) {
//            throw new BadRequestException("Illegal id");
//        }

        if(user.getRole().getName().equals(RoleName.ROLE_USER)) {
            editClient(user, request);
        }

        ValidationUtil.validateTelephoneNumber(request.getNumber());
        user.setNumber(request.getNumber());

        if(request.getAddress() != null && !request.getAddress().isEmpty()) {
            user.setAddress(request.getAddress());
        }

        userRepo.save(user);
        return user;
    }

    private void editClient(AppUser user, EditRequest request) {
        request.setRegistration(request.getRegistration().toUpperCase());
        ValidationUtil.validateCarInfo(request.getCarModel(), request.getRegistration(), request.getProductionYear());

        user.setRegistration(request.getRegistration());
        user.setProductionYear(request.getProductionYear());
        user.setCarModel(request.getCarModel());
    }
}
