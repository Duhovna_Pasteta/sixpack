package fer.opp.autoservis.service.impl;

import fer.opp.autoservis.dao.RoleRepository;
import fer.opp.autoservis.dao.UserRepository;
import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.Role;
import fer.opp.autoservis.domain.RoleName;
import fer.opp.autoservis.exception.AppException;
import fer.opp.autoservis.exception.RequestDeniedException;
import fer.opp.autoservis.exception.ResourceNotFoundException;
import fer.opp.autoservis.security.UserDetailsImpl;
import fer.opp.autoservis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RoleRepository roleRepo;

    @Override
    public AppUser one(Long id) {
        return userRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
    }

    @Override
    public List<AppUser> listAll() {
        return userRepo.findAll();
    }

    @Override
    public AppUser myProfile() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long id = ((UserDetailsImpl) principal).getId();
        AppUser currentUser = userRepo.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("User", "id", id));
        return currentUser;
    }
}
