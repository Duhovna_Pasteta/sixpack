package fer.opp.autoservis.service.impl;

import fer.opp.autoservis.dao.RoleRepository;
import fer.opp.autoservis.dao.SubstituteCarRepository;
import fer.opp.autoservis.dao.UserRepository;
import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.SubstituteCar;
import fer.opp.autoservis.exception.BadRequestException;
import fer.opp.autoservis.exception.RequestDeniedException;
import fer.opp.autoservis.exception.ResourceNotFoundException;
import fer.opp.autoservis.security.UserDetailsImpl;
import fer.opp.autoservis.service.SubstituteCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO Zastititi sucelje
@Service
public class SubstituteCarServiceImpl implements SubstituteCarService {

    @Autowired
    private SubstituteCarRepository carRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RoleRepository roleRepo;

    public SubstituteCar fetchMy() {
        AppUser currentUser = getCurrentUser();
        List<SubstituteCar> list = carRepo.findByUser(currentUser);
        if (list.isEmpty()) {
            throw new RequestDeniedException("Korisnik nije rezervirao zamjensko vozilo!");
        }
        return list.get(0);
    }

    public List<SubstituteCar> fetchAll() {
        return carRepo.findAll();
    }

    public List<SubstituteCar> fetchAvailableCars() {
        return carRepo.findAllByUserIsNull();
    }

    public SubstituteCar reserve(Long carId) {
        AppUser currentUser = getCurrentUser();
        List<SubstituteCar> list = carRepo.findByUser(currentUser);
        if (!list.isEmpty()) {
            throw new RequestDeniedException("Korisnik može rezervirati samo jedno zamjensko vozilo");
        }
        SubstituteCar car = carRepo.findById(carId)
                .orElseThrow(() -> new ResourceNotFoundException("Substitute car", "id", carId));
        if (car.getUser() != null) {
            throw new RequestDeniedException("Zamjensko vozilo je već rezervirano!");
        }
        car.setUser(currentUser);
        carRepo.save(car);
        return car;
    }

    public SubstituteCar free(long carId) {
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        Long id = ((UserDetailsImpl) principal).getId();
//        AppUser currentUser = userRepo.findById(id).orElseThrow(() ->
//                new ResourceNotFoundException("User", "id", id));
////        if (currentUser.getRole().getName() != RoleName.ROLE_USER) {
////            throw new RequestDeniedException("Samo korisnici mogu osloboditi rezervirani auto!");
////        }
//        List<SubstituteCar> list = carRepo.findByUser(currentUser);
//        if (list.isEmpty()) {
//            throw new RequestDeniedException("Korisnik nema rezerviranih automobila");
//        }
//        SubstituteCar car = list.get(0);
//        car.setUser(null);
//        carRepo.save(car);
//        return car;
        SubstituteCar car = carRepo.findById(carId).orElseThrow(() ->
                new BadRequestException("Auto s identifikacijskom oznakom: " + carId + " ne postoji"));

        car.setUser(null);
        return carRepo.save(car);
    }

    public SubstituteCar add(SubstituteCar car) {
        carRepo.findByRegistration(car.getRegistration())
                .ifPresent((t) -> {
                    throw new RequestDeniedException("Auto s tom registracijom već postoji!");
                });
        car.setId(null);
        carRepo.save(car);
        return car;
    }

    @Override
    public SubstituteCar fetchOne(long carId) {
        return carRepo.findById(carId).orElseThrow(() ->
                new BadRequestException("Auto s identifikacijskom oznakom: " + carId + " ne postoji"));
    }


    private AppUser getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long id = ((UserDetailsImpl) principal).getId();
        return userRepo.findById(id).orElseThrow(() ->  new ResourceNotFoundException("User", "id", id));
    }

}
