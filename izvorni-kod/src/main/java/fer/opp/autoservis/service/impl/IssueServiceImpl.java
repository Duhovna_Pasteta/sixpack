package fer.opp.autoservis.service.impl;

import fer.opp.autoservis.dao.IssueRepository;
import fer.opp.autoservis.domain.CarIssue;
import fer.opp.autoservis.service.IssueService;
import fer.opp.autoservis.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IssueServiceImpl implements IssueService {

    @Autowired
    private IssueRepository issueRepo;

    @Override
    public List<CarIssue> listAll() {
        return issueRepo.findAll();
    }

    @Override
    public CarIssue create(String description) {
        description = description.trim();
        if(description.length() == 0) {
            throw new BadRequestException("Opis problema ne smije biti prazan");
        }

        return issueRepo.save(new CarIssue(description.replace("\n", "")));
    }
}