package fer.opp.autoservis.service;

import fer.opp.autoservis.domain.Appointment;
import fer.opp.autoservis.payload.AppointmentRequest;
import fer.opp.autoservis.payload.AppointmentReservationInfo;
import fer.opp.autoservis.payload.ReservedAppointmentInfo;

import java.util.List;

public interface AppointmentService {

//    Appointment bookAppointment(AppointmentRequest request);

    Appointment bookAppointment(AppointmentRequest request);

    Appointment fetch(long id);

    List<ReservedAppointmentInfo> fetchRepairersReservedAppointments(long repairerId);

    List<ReservedAppointmentInfo> getAllReservedAppointments();

//    void addIssue(long appointmentId, long issueId);

    void deleteAppointment(Long appointmentId);

    Appointment getMyAppointment();
}
