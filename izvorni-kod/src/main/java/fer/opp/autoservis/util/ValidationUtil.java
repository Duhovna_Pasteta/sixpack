package fer.opp.autoservis.util;

import fer.opp.autoservis.exception.BadRequestException;

public class ValidationUtil {

    public static void validateCarInfo(String carModel, String registration, String productionYear) {
        if(carModel == null || carModel.trim().length() == 0) {
            throw new BadRequestException("Model auta ne smije biti prazan");
        }

        String carRegistrationRegex = "^[\\p{L}]{2}[ ,-][0-9]{3,4}[ ,-][A-Z]{2}";
        if(registration == null || !registration.matches(carRegistrationRegex)) {
            throw new BadRequestException("Registracija nije ispravnog oblika");
        }

        if(productionYear == null || productionYear.trim().length() != 4) {
            throw new BadRequestException("Godina prooizvodnje je obavezna");
        }
    }

    public static void validateTelephoneNumber(String number) {
        isNumber(number);

        if(number.length() == 9 || number.length() == 10) {
            if(!number.startsWith("0")) {
                throw new BadRequestException("Telefonski broj je krivog formata");
            }
        } else if(number.length() != 6) {
            throw new BadRequestException("Telefonski broj je krive dužine");
        }
    }

    private static void isNumber(String number) {
        try {
            Long.parseLong(number);
        } catch (NumberFormatException ex) {
            throw new BadRequestException("Telefonski broj je krivog formata");
        }
    }
}
