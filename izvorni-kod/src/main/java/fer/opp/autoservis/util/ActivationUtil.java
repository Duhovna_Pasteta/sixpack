package fer.opp.autoservis.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

public class ActivationUtil {

    public static void sendActivationMail(String mail, String activationValue, JavaMailSender emailSender) {
        Runnable sendMail = () -> {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setSubject("Aktivacija korisničkog računa");
            message.setTo(mail);
            message.setText("Za aktivaciju korisničkog računa molimo klilknite na sljedeću poveznicu: " +
                    "https://autoservis.ml/api/auth/activate/" + activationValue);

            emailSender.send(message);
        };
        Thread t = new Thread(sendMail);
        t.start();
    }

    public static String generateActivationValue() {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        return RandomStringUtils.random(50, characters );
    }
}
