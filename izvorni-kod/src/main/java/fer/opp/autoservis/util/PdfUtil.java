package fer.opp.autoservis.util;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import fer.opp.autoservis.domain.Appointment;
import fer.opp.autoservis.exception.BadRequestException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDate;

public class PdfUtil {

    public static ByteArrayInputStream appointmentConfirmation(Appointment appointment) {
        if(appointment.isFree()) {
            throw new BadRequestException("Can't generate confirmation for free appointment");
        }
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            Paragraph title = new Paragraph();
            title.setAlignment(Element.ALIGN_CENTER);

            Font titleFont = FontFactory.getFont(FontFactory.TIMES, 16, BaseColor.BLACK);
            Chunk chunk = new Chunk("AutoServis Najbolji Mehaničar", titleFont);
            title.add(chunk);

            Paragraph text = new Paragraph();
            text.setFirstLineIndent(20);
            text.setSpacingBefore(70);
            Font textFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK);
            chunk = new Chunk("U stanju zdravog razuma i svjesnog uma, ja " + appointment.getClient().getName() +
                    " " +  appointment.getClient().getSurname() + " potvrđujem da sam predao\\la svoj automobil u " +
                    "AutoServis Najbolji Mehaničar na popravak.", textFont);
            text.add(chunk);

            Paragraph footer = new Paragraph(20);
            footer.setSpacingBefore(600);
            LocalDate hlp = appointment.getAppointmentDateTime().toLocalDate();
            chunk = new Chunk("\tPotpis klijenta: _____________________                  " +
                    "Potpis servisera: _____________________\n\tDatum: " +
                    String.format("%d.%d.%d.",hlp.getDayOfMonth(), hlp.getMonthValue(), hlp.getYear()), textFont);
            footer.add(chunk);

            PdfWriter.getInstance(document, out);
            document.open();

            document.add(title);
            document.add(text);
            document.add(footer);

            document.close();

        } catch (DocumentException ignored) {
        }

        return new ByteArrayInputStream(out.toByteArray());
    }
}
