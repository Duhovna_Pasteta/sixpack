package fer.opp.autoservis.util;

import fer.opp.autoservis.domain.Appointment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.concurrent.Callable;

public class MailCallable implements Callable<Void> {

    private Appointment appointment;

    private JavaMailSender emailSender;

    public MailCallable(Appointment appointment, JavaMailSender emailSender) {
        this.appointment = appointment;
        this.emailSender = emailSender;
    }

    @Override
    public Void call() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(appointment.getClient().getMail());
        message.setSubject("Predaja vozila na popravak");
        message.setText("Poštovani,\n\npodsjećamo Vas da je Vaš termin za predaju vozila u AutoServis Najbolji " +
                "Mehaničar je sutra.\nTočan datum i vrijeme vašeg termina su " + appointment.getAppointmentDateTime().toString().replace("T", " ") +
                "\n\n Lijep pozdrav,\nVaš najdraži AutoServis Najbolji Mehaničar!");
        emailSender.send(message);
        return null;
    }
}
