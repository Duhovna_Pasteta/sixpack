package fer.opp.autoservis.util;

import fer.opp.autoservis.dao.AppointmentRepository;
import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.Appointment;

import java.time.LocalDateTime;

public class RepairerUtil {

    public static void createAppointments(AppointmentRepository appointmentRepo, AppUser repairer, LocalDateTime date) {
        for(int hour = 7; hour < 10; hour++)
            for (int minute = 0; minute < 60; minute += 20) {
                Appointment appointment = new Appointment(LocalDateTime.of(date.getYear(), date.getMonth(), date.getDayOfMonth(), hour, minute, 0), repairer);
                appointmentRepo.save(appointment);
            }
    }
}
