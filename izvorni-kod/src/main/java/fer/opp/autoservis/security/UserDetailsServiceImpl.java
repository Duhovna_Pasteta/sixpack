package fer.opp.autoservis.security;

import fer.opp.autoservis.dao.UserRepository;
import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Loads a user’s data given its username.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepo;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
        AppUser user = userRepo.findByMail(mail).
                orElseThrow(() -> new BadRequestException("User with email: " + mail + " not found"));

        return UserDetailsImpl.create(user);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        AppUser user = userRepo.findById(id).
                orElseThrow(() -> new BadRequestException("User with id: " + id + " not found"));

        return UserDetailsImpl.create(user);
    }
}
