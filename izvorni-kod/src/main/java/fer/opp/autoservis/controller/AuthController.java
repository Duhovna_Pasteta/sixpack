package fer.opp.autoservis.controller;

import fer.opp.autoservis.payload.LoginResponse;
import fer.opp.autoservis.payload.RegistrationRequest;
import fer.opp.autoservis.payload.RegistrationResponse;
import fer.opp.autoservis.payload.LoginRequest;
import fer.opp.autoservis.service.ActivationService;
import fer.opp.autoservis.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @Autowired
    private ActivationService activationService;

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok(authService.login(loginRequest));
    }

    @PostMapping("/register")
    public ResponseEntity<RegistrationResponse> registerUser(@Valid @RequestBody RegistrationRequest registrationRequest) {
        RegistrationResponse response = authService.register(registrationRequest);

        if(response.getSuccess()) {
            return ResponseEntity.created(URI.create("/users/me")).body(response);
        } else {
            return ResponseEntity.badRequest().body(response);
        }
    }

    @GetMapping("/activate/{value}")
    public ResponseEntity<String> activateMail(@PathVariable("value") String value, HttpServletResponse http) throws IOException {
        String redirect = activationService.activateUser(value);

        http.sendRedirect(redirect);
        return ResponseEntity.ok("Korisnik je aktiviran");
    }
}
