package fer.opp.autoservis.controller;

import fer.opp.autoservis.domain.SubstituteCar;
import fer.opp.autoservis.service.SubstituteCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cars")
public class SubstituteCarController {

    @Autowired
    private SubstituteCarService carService;

    /**
     * Dohvaća rezervirani auto prijavljenog korisnika.
     * @return Objekt tipa SubstituteCar koji sadrži informacije o rezerviranom autu.
     */
    @GetMapping("/my")
    @Secured("ROLE_USER")
    public SubstituteCar fromUser() {
        return carService.fetchMy();
    }

    /**
     * Dohvaća listu svih zamjenskih auta u bazi podataka.
     * @return Lista objekata tipa SubstituteCar koji sadrže informacije o autima.
     */
    @GetMapping("/all")
    @Secured("ROLE_ADMIN")
    public List<SubstituteCar> getAll() {
        return carService.fetchAll();
    }

    /**
     * Dohvaća listu slobodnih zamjenskih auta koje korisnici mogu rezervirati.
     * @return Lista objekata tipa SubstituteCar koji sadrže informacije o slobodnim autima za rezervaciju.
     */
    @GetMapping("/available")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public List<SubstituteCar> getAvailable() {
        return carService.fetchAvailableCars();
    }

    /**
     * Dodaje novo zamjensko vozilo u bazu podataka koje će korisnici moći rezervirati.
     * @param car Objekt tipa SubstituteCar koji sadrži informacije o novom zamjenskom autu.
     * @return Objekt tipa SubstituteCar koji sadrži informacije o dodanom zamjenskom autu.
     */
    @PostMapping("/add")
    @Secured("ROLE_ADMIN")
    public SubstituteCar addCar(@RequestBody SubstituteCar car) {
        carService.add(car);
        return car;
    }

    /**
     * Rezervira zamjenski auto za trenutno prijavljenog korisnika.
     * @param carId Id zamjenskog auta kojeg korisnik želi rezervirati.
     * @return Objekt SubstituteCar koji sadrži informacije o rezerviranom autu.
     */
    @GetMapping("/reserve/{carId}")
    @Secured("ROLE_USER")
    public SubstituteCar reserveCar(@PathVariable("carId") Long carId) {
        return carService.reserve(carId);
    }

    /**
     * Oslobađa zamjenski auto kojeg je prijavljeni korisnik prije rezervirao.
     * @return Objekt tipa SubstituteCar koji sadrži nove informacije o autu kojeg je korisnik oslobodio.
     */
    @GetMapping("/free/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<SubstituteCar> freeCar(@PathVariable("id") long carId) {
        return  ResponseEntity.ok().body(carService.free(carId));
    }

    @GetMapping("/one/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<SubstituteCar> fetch(@PathVariable("id") long carId) {
        return ResponseEntity.ok().body(carService.fetchOne(carId));
    }
}
