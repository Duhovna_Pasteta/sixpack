package fer.opp.autoservis.controller;

import fer.opp.autoservis.domain.Comment;
import fer.opp.autoservis.payload.CommentRequest;
import fer.opp.autoservis.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping
    @Secured({"ROLE_USER", "ROLE_REPAIRER"})
    public Comment addComment(@Valid @RequestBody CommentRequest request) {
        return commentService.addComment(request);
    }
}
