package fer.opp.autoservis.controller;

import fer.opp.autoservis.domain.AutoService;
import fer.opp.autoservis.service.AutoServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/service-info")
public class AutoServiceController {

    @Autowired
    private AutoServiceService service;

    /**
     * Dohvaća objekt tipa AutoService koji sadrži informacije o autoservisu.
     */
    @GetMapping("/get")
    public AutoService get() {
        return service.fetch();
    }

    /**
     * Ažurira informacije o autoservisu.
     * @param as AutoService objekt koji sadrži sve informacije o autoservisu (varijabla id navedenog objekta
     *           može biti bilo koja vrijednost)
     * @return Objekt tipa AutoService sa svim informacijama o autoservisu
     */
    @Secured("ROLE_ADMIN")
    @PostMapping("/update")
    public AutoService update(@RequestBody AutoService as) {
        return service.update(as);
    }

    /**
     * Dodaje početni objekt s informacijama tipa AutoService u bazu podataka. Ovo neće biti
     * potrebno izvšiti s frontenda jer će obično takav objekt već postojati u bazi podataka.
     * @param as Objekt sa svim informacijama o autoservisu.
     * @return Novi objekt sa svim informacijama o autoservisu.
     */
    @Secured("ROLE_ADMIN")
    @PostMapping("/add")
    public AutoService add(@RequestBody AutoService as) {
        return service.add(as);
    }

}
