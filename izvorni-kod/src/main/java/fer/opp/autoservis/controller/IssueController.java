package fer.opp.autoservis.controller;

import fer.opp.autoservis.domain.CarIssue;
import fer.opp.autoservis.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/issue")
public class IssueController {

    @Autowired
    private IssueService issueService;

    @GetMapping("")
    public List<CarIssue> listIssues() {
        return issueService.listAll();
    }

    @PostMapping("")
    @Secured("ROLE_ADMIN")
    public CarIssue createIssue(@RequestBody String description) {
        return issueService.create(description);
    }
}
