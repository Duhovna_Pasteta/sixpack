package fer.opp.autoservis.controller;

import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.payload.ChangePasswordRequest;
import fer.opp.autoservis.service.PasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@RestController
@RequestMapping("/password")
public class PasswordController {

    @Autowired
    private PasswordService passwordService;

    @PostMapping("")
    public void forgottenPassword(@RequestBody @NotEmpty String mail) {
        passwordService.forgottenPassword(mail);
    }

    @PostMapping("change")
    @Secured({"ROLE_USER", "ROLE_ADMIN", "ROLE_REPAIRER"})
    public ResponseEntity<AppUser> changePassword(@Valid @RequestBody ChangePasswordRequest request) {
        return ResponseEntity.ok().body(passwordService.changePassword(request));
    }
}
