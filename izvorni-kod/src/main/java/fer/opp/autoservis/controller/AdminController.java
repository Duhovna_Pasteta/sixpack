package fer.opp.autoservis.controller;

import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.payload.EditRequest;
import fer.opp.autoservis.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @PostMapping("/edit/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<AppUser> editRepairer(@PathVariable("id") long userId, @Valid @RequestBody EditRequest request) {
        return ResponseEntity.ok().body(adminService.editUser(userId, request));
    }
}
