package fer.opp.autoservis.controller;

import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.Appointment;
import fer.opp.autoservis.payload.FreeAppointmentInfo;
import fer.opp.autoservis.payload.RegistrationRequest;
import fer.opp.autoservis.payload.RegistrationResponse;
import fer.opp.autoservis.service.RepairerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.io.ByteArrayInputStream;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/repairers")
public class RepairerController {

    @Autowired
    private RepairerService repairerService;

    @GetMapping("")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<List<AppUser>> listRepairers() {
//        return repairerService.listAll();
        return ResponseEntity.ok().body(repairerService.listAll());
    }

    @GetMapping("/appointments/{id}")
    @Secured("ROLE_USER")
    public ResponseEntity<List<FreeAppointmentInfo>> getFreeAppointmentTimes(@PathVariable("id") long repairerId) {
//        return repairerService.getFreeAppointments(repairerId);
        return ResponseEntity.ok(repairerService.getFreeAppointmentTimes(repairerId));
    }

    @PostMapping("/create")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> createRepairer(@Valid @RequestBody RegistrationRequest request) {
        RegistrationResponse response = repairerService.createRepairer(request);

        if(response.getSuccess()) {
            return ResponseEntity.created(URI.create("/users/" + response.getUserId())).body(response);
        } else {
            return ResponseEntity.badRequest().body(response);
        }
    }

    @GetMapping(value = "/pdf/{id}", produces = MediaType.APPLICATION_PDF_VALUE)
    @Secured("ROLE_REPAIRER")
    public ResponseEntity<InputStreamResource> generatePdfFile(@PathVariable("id") long appointmentId) {
        ByteArrayInputStream bis = repairerService.appointmentConfirmation(appointmentId);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=Potvrda.pdf");

        return ResponseEntity.
                ok().
                headers(headers).
                contentType(MediaType.APPLICATION_PDF).
                body(new InputStreamResource(bis));
    }

    @PostMapping("/carStatus/{id}")
    @Secured("ROLE_REPAIRER")
    public ResponseEntity<Appointment> changeCarStatus(@PathVariable("id") long appointmentId, @RequestBody @NotEmpty String status) {
        return ResponseEntity.ok().body(repairerService.changeCarStatus(appointmentId, status));
    }
}
