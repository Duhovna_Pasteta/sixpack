package fer.opp.autoservis.controller;

import fer.opp.autoservis.domain.Appointment;
import fer.opp.autoservis.payload.AppointmentRequest;
import fer.opp.autoservis.payload.AppointmentReservationInfo;
import fer.opp.autoservis.payload.ReservedAppointmentInfo;
import fer.opp.autoservis.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/appointment")
public class AppointmentController {

    @Autowired
    private AppointmentService appointmentService;

    @PostMapping("/book")
    @Secured("ROLE_USER")
    public Appointment bookAppointment(@Valid @RequestBody AppointmentRequest request) {
        return appointmentService.bookAppointment(request);
    }

    @GetMapping("/reserved")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<List<ReservedAppointmentInfo>> getAllReservedAppointments() {
        return ResponseEntity.ok().body(appointmentService.getAllReservedAppointments());
    }


    // Vraća jedan određeni termin
    @GetMapping("/{id}")
    @Secured({"ROLE_USER", "ROLE_REPAIRER", "ROLE_ADMIN"})
    public Appointment getAppointment(@PathVariable("id") long id) {
        return appointmentService.fetch(id);
    }

    @GetMapping("/my")
    @Secured("ROLE_USER")
    public ResponseEntity<Appointment> getMyAppointment() {
        Appointment appointment = appointmentService.getMyAppointment();
        if(appointment == null) {
            return ResponseEntity.badRequest().body(null);
        } else {
            return ResponseEntity.ok().body(appointment);
        }
    }

    // Vraća zauzete termine određenog servisera
    @GetMapping("/repairer/{id}")
    @Secured({"ROLE_USER", "ROLE_REPAIRER"})
    public List<ReservedAppointmentInfo> getReservedAppointments(@PathVariable("id") Long repairerId) {
        return appointmentService.fetchRepairersReservedAppointments(repairerId);
    }

    @DeleteMapping("/{id}")
    @Secured({"ROLE_USER", "ROLE_ADMIN", "ROLE_REPAIRER"})
    public void deleteAppointment(@PathVariable("id") Long appointmentId) {
         appointmentService.deleteAppointment(appointmentId);
    }
}
