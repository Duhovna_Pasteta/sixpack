package fer.opp.autoservis.controller;

import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    /**
     * Dohvaća sve korisnike sustava koji su pohranjeni u bazi podataka.
     * @return Lista AppUser objekata koji sadrže informacije o korisnicima.
     */
    @GetMapping("/all")
    @Secured("ROLE_ADMIN")
    public List<AppUser> listUsers() {
        return userService.listAll();
    }

    /**
     * Dohvaća profil korisnika s id-om navedenim u URI-u.
     * @param id Id korisnika čiji se profil traži.
     * @return
     */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public AppUser displayUser(@PathVariable Long id) {
        return userService.one(id);
    }

    /**
     * Dohvaća profil trenutno prijavljenog korisnika.
     * @return AppUser objekt koji sadrži informacije o trenutno prijavljenom korisniku.
     */
    @RequestMapping("/me")
    @Secured({"ROLE_ADMIN", "ROLE_REPAIRER", "ROLE_USER"})
    public AppUser me() {
        return userService.myProfile();
    }

}
