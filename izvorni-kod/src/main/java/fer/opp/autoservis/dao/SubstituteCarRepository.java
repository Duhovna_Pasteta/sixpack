package fer.opp.autoservis.dao;

import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.SubstituteCar;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubstituteCarRepository extends JpaRepository<SubstituteCar, Long> {

    Optional<SubstituteCar> findById(Long id);

    Optional<SubstituteCar> findByRegistration(String registration);

    List<SubstituteCar> findByUser(AppUser user);

    List<SubstituteCar> findAllByUserIsNull();

    List<SubstituteCar> findByManufacturer(String manufacturer);

}
