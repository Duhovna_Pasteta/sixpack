package fer.opp.autoservis.dao;

import fer.opp.autoservis.domain.PendingRegistration;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PendingRegistrationRepository extends JpaRepository<PendingRegistration, Long> {

    Optional<PendingRegistration> findByActivationValue(String value);

    boolean existsByMail(String mail);
}
