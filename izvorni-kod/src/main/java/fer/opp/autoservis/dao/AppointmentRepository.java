package fer.opp.autoservis.dao;

import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

//    boolean existsByAppointmentDateTimeAndRepairer(LocalDateTime dateTime, AppUser repairer);

    Optional<Appointment> findById(Long id);

    List<Appointment> findAllByRepairer(AppUser repairer);

    List<Appointment> findAllByClient(AppUser client);

    List<Appointment> findAllByFreeAndRepairer(boolean free, AppUser repairer);

    List<Appointment> findAllByFree(boolean free);
}
