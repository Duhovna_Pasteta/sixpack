package fer.opp.autoservis.dao;

import fer.opp.autoservis.domain.AutoService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AutoServiceRepository extends JpaRepository<AutoService, Long> {

    List<AutoService> findAll();
}
