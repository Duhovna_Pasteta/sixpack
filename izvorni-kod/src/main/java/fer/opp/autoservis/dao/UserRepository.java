package fer.opp.autoservis.dao;

import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> findByMail(String mail);

    Boolean existsByMail(String mail);

    Boolean existsByRegistration(String registration);

    List<AppUser> findAllByRole(Role role);

    Optional<AppUser> findAllByRoleAndId(Role role, Long id);

    Optional<AppUser> findById(Long id);
}
