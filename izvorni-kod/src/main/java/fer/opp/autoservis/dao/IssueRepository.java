package fer.opp.autoservis.dao;

import fer.opp.autoservis.domain.CarIssue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IssueRepository extends JpaRepository<CarIssue, Long> {

    Optional<CarIssue> findById(Long id);
}
