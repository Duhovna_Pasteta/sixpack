package fer.opp.autoservis.dao;

import fer.opp.autoservis.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
}
