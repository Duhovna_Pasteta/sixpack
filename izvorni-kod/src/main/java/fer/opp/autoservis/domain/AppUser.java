package fer.opp.autoservis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fer.opp.autoservis.payload.RegistrationRequest;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "users")
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique = true)
    @Size(min = 5, max = 50)
    @Email
    private String mail;

    @NotNull
    @JsonIgnore
    @Size(min = 6, max = 100)
    private String password;

    @NotNull
    @Size(min = 2, max = 50)
    private String name;

    @NotNull
    @Size(min = 2, max = 50)
    private String surname;

    @Size(max = 10)
    private String number;

    @Size(max = 100)
    private String address;

    @JsonIgnore
    @ManyToOne
    @NotNull
    private Role role;

    private String carModel;

    @Column(unique = true)
    private String registration;

    private String productionYear;

    public AppUser() {
    }

    public AppUser(RegistrationRequest request) {
        this.mail = request.getMail();
        this.password = request.getPassword();
        this.name = request.getName();
        this.surname = request.getSurname();
        this.number = request.getNumber();
        this.address = request.getAddress();
        this.carModel = request.getCarModel();
        this.registration = request.getRegistration();
        this.productionYear = request.getProductionYear();
    }

    public AppUser(String mail, String password, String name, String surname, String number, String address) {
        this.mail = mail;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.number = number;
        this.address = address;
    }

    public AppUser(PendingRegistration pending) {
        this.mail = pending.getMail();
        this.name = pending.getName();
        this.surname = pending.getSurname();
        this.password = pending.getPassword();
        this.number = pending.getNumber();
        this.address = pending.getAddress();
        this.carModel = pending.getCarModel();
        this.registration = pending.getRegistration();
        this.productionYear = pending.getProductionYear();
        this.role = pending.getRole();
    }

    public Long getId() {
        return this.id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(String productionYear) {
        this.productionYear = productionYear;
    }
}
