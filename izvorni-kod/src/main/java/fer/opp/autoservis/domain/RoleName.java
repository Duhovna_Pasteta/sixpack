package fer.opp.autoservis.domain;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_REPAIRER,
    ROLE_USER
}
