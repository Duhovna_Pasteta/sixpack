package fer.opp.autoservis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fer.opp.autoservis.payload.RegistrationRequest;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity(name = "pending_registrations")
public class PendingRegistration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String activationValue;

    @NotNull
    @Column(unique = true)
    @Size(min = 5, max = 50)
    @Email
    private String mail;

    @NotNull
    @JsonIgnore
    @Size(min = 6, max = 100)
    private String password;

    @NotNull
    @Size(min = 2, max = 50)
    private String name;

    @NotNull
    @Size(min = 2, max = 50)
    private String surname;

    @Size(max = 10)
    private String number;

    @Size(max = 100)
    private String address;

    @JsonIgnore
    @ManyToOne
    @NotNull
    private Role role;

    private String carModel;

    @Column(unique = true)
    private String registration;

    private String productionYear;

    private LocalDate requestTime;

    public PendingRegistration() {
        this.requestTime = LocalDate.now();
    }

    public PendingRegistration(RegistrationRequest request) {
        this();
        this.mail = request.getMail();
        this.password = request.getPassword();
        this.name = request.getName();
        this.surname = request.getSurname();
        this.number = request.getNumber();
        this.address = request.getAddress();
        this.carModel = request.getCarModel();
        this.registration = request.getRegistration();
        this.productionYear = request.getProductionYear();
    }

    public Long getId() {
        return id;
    }

    public String getActivationValue() {
        return activationValue;
    }

    public void setActivationValue(String activationValue) {
        this.activationValue = activationValue;
    }

    public String getMail() {
        return mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getNumber() {
        return number;
    }

    public String getAddress() {
        return address;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getRegistration() {
        return registration;
    }

    public String getProductionYear() {
        return productionYear;
    }

    public LocalDate getRequestTime() {
        return requestTime;
    }
}
