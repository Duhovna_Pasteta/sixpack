package fer.opp.autoservis.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Appointment {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME, pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "dd.MM.yyyy. HH:mm:ss")
    private LocalDateTime appointmentDateTime;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME, pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "dd.MM.yyyy. HH:mm:ss")
    private LocalDateTime reservationTime;

    @ManyToOne
    private AppUser client;

    @ManyToOne
    private AppUser repairer;

    private String additionalUserDescription;

    private String carStatus;

    private boolean free;

    @OneToMany
    @JoinTable(name = "appointment_comments",
            joinColumns = @JoinColumn(name = "appointment_id"),
            inverseJoinColumns = @JoinColumn(name = "comment_id"))
    private List<Comment> comments;

    @ManyToMany
    @JoinTable(name = "appointment_issues",
            joinColumns = @JoinColumn(name = "appointment_id"),
            inverseJoinColumns = @JoinColumn(name = "issue_id"))
    private List<CarIssue> carIssues;

    public Appointment() {
        this.comments = new LinkedList<>();
        this.carIssues = new LinkedList<>();
        this.free = true;
        this.carStatus = "Vozilo nije predano";
    }

    public Appointment(LocalDateTime appointmentDateTime, AppUser repairer) {
        this();
        this.appointmentDateTime = appointmentDateTime;
        this.repairer = repairer;
    }

    public LocalDateTime getAppointmentDateTime() {
        return appointmentDateTime;
    }

    public LocalDateTime getReservationTime() {
        return reservationTime;
    }

    public void setReservationTime(LocalDateTime reservationTime) {
        this.reservationTime = reservationTime;
    }

    public Long getId() {
        return id;
    }

    public AppUser getClient() {
        return client;
    }

    public void setClient(AppUser client) {
        this.client = client;
    }

    public AppUser getRepairer() {
        return repairer;
    }

    public void setRepairer(AppUser repairer) {
        this.repairer = repairer;
    }

    public String getAdditionalUserDescription() {
        return additionalUserDescription;
    }

    public void setAdditionalUserDescription(String additionalUserDescription) {
        this.additionalUserDescription = additionalUserDescription;
    }

    public void setCarStatus(String carStatus) {
        this.carStatus = carStatus;
    }

    public String getCarStatus() {
        return carStatus;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<CarIssue> getCarIssues() {
        return carIssues;
    }

    public void setCarIssues(List<CarIssue> carIssues) {
        this.carIssues = carIssues;
    }

    public void addComment(Comment comment) {
        this.comments.add(comment);
    }

    public void addIssue(CarIssue issue) {
        this.carIssues.add(issue);
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }
}
