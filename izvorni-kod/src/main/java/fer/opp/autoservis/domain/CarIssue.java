package fer.opp.autoservis.domain;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity(name = "car_issues")
public class CarIssue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Column(unique = true)
    private String description;

    public CarIssue() {
    }

    public CarIssue(@NotEmpty String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
