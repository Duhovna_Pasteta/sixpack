package fer.opp.autoservis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity(name = "cars")
public class SubstituteCar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String manufacturer;

    @NotNull
    private String model;

    @Size(min = 4, max = 4)
    private String year;

    @NotNull
    @Column(unique = true)
    private String registration;

    @OneToOne
    @JsonIgnore
    private AppUser user;

    private Long registeredById;

    private String registeredBy;

    public SubstituteCar() {
        registeredBy = "Slobodno";
    }

    public SubstituteCar(String manufacturer, String model, String registration, String year) {
        this();
        this.manufacturer = manufacturer;
        this.model = model;
        this.year = year;
        this.registration = registration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public String getYear() {
        return year;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public AppUser getUser() {
        return user;
    }

    public void setUser(AppUser user) {
        this.user = user;

        if(user != null) {
            this.registeredById = user.getId();
            this.registeredBy = user.getName() + " " + user.getSurname();
        } else {
            this.registeredById = null;
            this.registeredBy = "Slobodno";
        }
    }

    public Long getRegisteredById() {
        return registeredById;
    }

    public String getRegisteredBy() {
        return registeredBy;
    }
}
