package fer.opp.autoservis.components;

import fer.opp.autoservis.dao.AppointmentRepository;
import fer.opp.autoservis.dao.PendingRegistrationRepository;
import fer.opp.autoservis.domain.AppUser;
import fer.opp.autoservis.domain.Appointment;
import fer.opp.autoservis.domain.PendingRegistration;
import fer.opp.autoservis.service.RepairerService;
import fer.opp.autoservis.util.MailCallable;
import fer.opp.autoservis.util.RepairerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Component
public class ScheduledTasks {

    @Autowired
    private RepairerService repairerService;

    @Autowired
    private AppointmentRepository appointmentRepo;

    @Autowired
    private PendingRegistrationRepository pendingRepo;

    @Autowired
    private JavaMailSender emailSender;

    private ExecutorService pool;

    public ScheduledTasks() {
        pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), r -> {
            Thread worker = new Thread(r);
            worker.setDaemon(true);
            return worker;
        });
    }

    @Scheduled(cron = "0 0 3 * * ?")
    public void discardUnconfirmedRegistrations() {
        LocalDate now = LocalDate.now();
        pendingRepo.findAll().stream().filter(r -> r.getRequestTime().plusDays(5).isBefore(now)).forEach(r -> pendingRepo.delete(r));
    }

    @Scheduled(cron = "0 0 5 * * ?")
    public void createAppointments() {
        LocalDateTime date = LocalDateTime.now().plusDays(10);
        if(date.getDayOfWeek().equals(DayOfWeek.SATURDAY) || date.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
            return;
        }

        List<AppUser> repairers = repairerService.listAll();

        for(AppUser repairer : repairers) {
            RepairerUtil.createAppointments(appointmentRepo, repairer, date);
        }
    }

    @Scheduled(cron = "0 0 4 * * ?")
    public void deleteAppointments() {
        LocalDateTime now = LocalDateTime.now();
        List<Appointment> appointments = appointmentRepo.findAllByFree(true);

        appointments.stream().filter(a -> a.getAppointmentDateTime().isBefore(now)).forEach(a -> appointmentRepo.delete(a));
    }

    @Scheduled(cron = "0 0 6 * * ?")
    public void sendMails() {
        int targetDay = LocalDate.now().plusDays(3).getDayOfMonth();

        List<Appointment> appointments = appointmentRepo.findAllByFree(false)
                .stream()
                .filter(a -> a.getAppointmentDateTime().getDayOfMonth() == targetDay)
                .filter(a -> a.getReservationTime().plusDays(3).toLocalDate().compareTo(a.getAppointmentDateTime().toLocalDate()) <= 0)
                .collect(Collectors.toList());

        List<Future<Void>> results = new ArrayList<>();

        for(Appointment appointment : appointments) {
            MailCallable work = new MailCallable(appointment, emailSender);
            results.add(pool.submit(work));
        }

        for(Future<Void> work : results) {
            try {
                work.get();
            } catch (InterruptedException | ExecutionException ignored) {
            }
        }
    }
}