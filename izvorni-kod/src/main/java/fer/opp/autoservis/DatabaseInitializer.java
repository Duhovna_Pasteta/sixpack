package fer.opp.autoservis;

import fer.opp.autoservis.dao.*;
import fer.opp.autoservis.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
public class DatabaseInitializer implements CommandLineRunner {

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private IssueRepository issueRepo;

    @Autowired
    private SubstituteCarRepository carRepo;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private AutoServiceRepository autoServiceRepo;

    @Override
    public void run(String... args) {
        Role adminRole = new Role(RoleName.ROLE_ADMIN);
        roleRepo.save(adminRole);

        Role repairerRole = new Role(RoleName.ROLE_REPAIRER);
        roleRepo.save(repairerRole);

        Role userRole = new Role(RoleName.ROLE_USER);
        roleRepo.save(userRole);

        AppUser admin = new AppUser();
        admin.setMail("admin@autoservis.ml");
        admin.setName("Carlos");
        admin.setSurname("Adminković");
        admin.setNumber("0991111222");
        admin.setAddress("Savska 44");
        admin.setPassword(encoder.encode("adminko"));
        admin.setRole(adminRole);
        userRepo.save(admin);

        AppUser testUser = new AppUser();
        testUser.setMail("otto.engelmann@fer.hr");
        testUser.setName("Otto");
        testUser.setSurname("Engelmann");
        testUser.setNumber("0991234567");
        testUser.setAddress("Zagreb, Cvjetno Naselje");
        testUser.setPassword(encoder.encode("volimPastete"));  // <3 :*
        testUser.setRole(userRole);
        testUser.setCarModel("Kumobil");
        testUser.setRegistration("PŽ-420-TT");
        testUser.setProductionYear("2016");
        userRepo.save(testUser);

        AutoService autoService = new AutoService();
        autoService.setFirmId("1234");
        autoService.setPin("1234567890");
        autoService.setIban("1234-12345678-12345678");
        autoService.setHeadquater("Unska 3, Zagreb");
        autoService.setLocation("Prisavlje 3, Zagreb");
        autoService.setName("Autoservis Najbolji Mehaničar");
        autoService.setPhone("01 1234 1234");
        autoServiceRepo.save(autoService);

        initializeCarIssues();
        initializeSubstituteCars();
    }

    private void initializeSubstituteCars() {
        try (BufferedReader fileReader = new BufferedReader(
                new InputStreamReader(
                        new ClassPathResource("substitute_cars.txt").getInputStream()))) {
            String line;
            while ((line = fileReader.readLine()) != null) {
                line = line.trim();
                String[] parameters = line.split(";");
                carRepo.save(new SubstituteCar(parameters[0], parameters[1], parameters[2], parameters[3]));
            }
        } catch (IOException e) {
            e .printStackTrace();
        }
    }

    private void initializeCarIssues() {
        try (BufferedReader fileReader = new BufferedReader(
                new InputStreamReader(
                        new ClassPathResource("car_problems.txt").getInputStream()))) {
            String line;
            while ((line = fileReader.readLine()) != null) {
                issueRepo.save(new CarIssue(line.trim()));
            }
        } catch (IOException e) {
            e .printStackTrace();
        }
    }
}